<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get(' /','BlogController@viewBlogs1');
Route::get(' /blog-details/{id}','BlogController@viewBlogsFront');



Route::get('/about', function () {
    return view('about');
});
// Route::get('/contact', function () {
//     return view('contact');
// });

Route::match(['get','post'],'/comments','BlogController@comments');
Route::match(['get','post'],'/contact','ContactController@contact');
Route::match(['get','post'],'/likes','BlogController@likes');


 
Route::match(['get','post'],'/contact','ContactController@contact');

Route::get('/gallery','IndexController@gallery');


 
Route::get('/blog{id}','BlogController@viewBlogs2');






Route::get('/blog19','IndexController@blog4');


 
Route::get('/webinar','IndexController@webinar');

Route::get('/webinar1','IndexController@webinar1');

Route::get('/webinar2','IndexController@webinar2');

Route::get('/cls', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return "Cache is cleared"; //Return anything
});


Route::get('/faqs','IndexController@faqs');

 

Route::get('/digital_marketing','IndexController@digital_marketing');

Route::get('/datascience_python_maharashtra','IndexController@datascience_python_maharashtra');
Route::get('/datascience_python_banglore','IndexController@datascience_python_banglore');

Route::get('/data_analytics','IndexController@data_analytics');

Route::get('/download','IndexController@download');

Route::match(['get','post'],'password/reset','UserController@forgotPassword');







Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/adminindex', 'AdminindexController@index')->name('adminindex');
// Route::match(['get', 'post'],'/forgot_password',[App\Http\Controllers\UserController::class,'forgot_password'])->name('forgot_password');


Route::group(['prefix'=>'/'],function(){
    Route::group(['middleware'=>'auth'],function(){
        // Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/admin', function () {
            return view('admin.adminindex');
        });


        // admin blog section
Route::match(['get','post'],'admin/add-blog','BlogController@addBlog');
Route::match(['get','post'],'admin/view-blogs','BlogController@viewBlogs');
Route::match(['get','post'],'admin/edit-blog/{id}','BlogController@editBlog');
Route::match(['get','post'],'admin/delete-blog/{id}','BlogController@deleteBlog');

// Route::match(['get', 'post'],'/password_setting',[App\Http\Controllers\UserController::class,'change_password'])->name('user_change_password');

    });
});




