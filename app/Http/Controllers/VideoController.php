<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use App\Video;
use Auth;
use Session;
use Image;

class VideoController extends Controller
{
    public function addVideo(Request $request){
    	if($request->isMethod('post')){
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;
    		$video = new Video;
    		$video->title = $data['title'];
    
    		$video->link = $data['link'];
            $video->save();
    		return redirect()->back()->with('flash_message_success','video has been Added Successfully');  
    	}
    	return view('admin.video.add_video');
    }

    public function editVideo(Request $request, $id){
     	if($request->isMethod('post')){
    		$data = $request->all();
    	 
             if(empty($data['title'])){
                $data['title']='';
            }
            if(empty($data['link'])){
                $data['link']='';
            }
 
            Video::where('id',$id)->update([ 'title'=>$data['title'],'link'=>$data['link'] ])->count();
            return redirect()->back()->with('flash_message_success','video updated Successfully.');
    	}
        $videoDetails = Video::where(['id'=>$id])->first(); 
     
        return view('admin.video.edit_video')->with(compact('videoDetails'));
        
    }



    

    public function deleteVideo(Request $request, $id){
     	Video::where(['id'=>$id])->delete();
    	return redirect('/admin/view_video/')->with('flash_message_success','video Deleted Successfully.');
    }

    public function viewVideo(Request $request){
    	$videos = Video::orderBy('id','DESC')->get();
    	return view('admin.video.view_video')->with(compact('videos'));
    }

    public function frontFetchVideo(Request $request){
        $allvideos = Video::orderBy('id','DESC')->get();
        // dd($allvideos);
    	return view('videos')->with(compact('allvideos'));
    
	}

}
