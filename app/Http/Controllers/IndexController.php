<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller

{
    public function about(){
    	return view('about');
    }
    
    public function layout(){
        return view('layout');
    }

    public function courses(){
    	return view('courses');
    }
     public function gallery(){
    	return view('gallery');
    }

    public function blog(){
    	return view('blog');
    }

     public function blog1(){
    	return view('blog1');
    }
    public function blog2(){
        return view('blog2');
    }
    public function blog3(){
        return view('blog3');
    }
    public function blog4(){
        return view('blog4');
    }
    public function blog5(){
        return view('blog5');
    }
    public function blog6(){
        return view('blog6');
    }
    public function blog7(){
        return view('blog7');
    }
    public function blog8(){
        return view('blog8');
    }
    public function blog9(){
        return view('blog9');
    }
    public function blog10(){
        return view('blog10');
    }
    public function blog11(){
        return view('blog11');
    }
    public function blog12(){
        return view('blog12');
    }
    public function blog13(){
        return view('blog13');
    }
    public function blog14(){
        return view('blog14');
    }
    public function blog15(){
        return view('blog15');
    }
    public function blog16(){
        return view('blog16');
    }
    public function blog17(){
        return view('blog17');
    }
    public function blog18(){
        return view('blog18');
    }
    public function blog19(){
        return view('blog19');
    }
    public function placement(){
    	return view('placement');
    }
    public function digital_marketing(){
    	return view('digital_marketing');
    }
    public function datascience_python_maharashtra(){
        return view('datascience_python');
    }
    public function datascience_python_banglore(){
        return view('datascience_python1');
    }
        public function data_analytics(){
        return view('data_analytics');
    }
    
    public function faqs(){
        return view('faqs');
    }
    public function videos(){
        return view('videos');
    }
    public function webinar(){
        return view('webinar');
    }
    public function webinar1(){
        return view('webinar1');
    }
    public function webinar2(){
        return view('webinar2');
    }

     public function download(){
        return view('download');
    }


    
    
}
