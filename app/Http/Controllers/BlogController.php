<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Image;
use Session;
use App\Comments;
use App\Like;

class BlogController extends Controller
{
    public function addBlog(Request $request){
    	if($request->isMethod('post')){
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;

			$blog = new Blog;

    		$blog->name = $data['name'];
    		$blog->content = $data['content'];
    		$blog->blog_date = $data['blog_date'];

    		if($request->hasFile('image')){

    			// $image_tmp = Input::file('image');    // not working in laravel 6.6
    			$image_tmp = $request->image;			// so used this
    			$filename = time() . '.'.$image_tmp->clientExtension(); //so used this

    			if($image_tmp->isValid()){
    				$extension = $image_tmp->getClientOriginalExtension();
    				$filename = rand(111,99999).'.'.$extension;
    				$image_path = 'images/backend_images/blog/'.$filename;

    				Image::make($image_tmp)->save($image_path);

    				$blog->image = $filename;
    			}
    		}
    		$blog->save();
    		return redirect()->back()->with('flash_message_success','Blog Added Successfully.');
    	}
    	return view('admin.blog.add_blog');
    }

    public function editBlog(Request $request, $id=null){
    	if($request->isMethod('post')){
    		$data = $request->all();

    		if($request->hasFile('image')){
     			$image_tmp = $request->image;			// so used this
    			$filename = time() . '.'.$image_tmp->clientExtension(); //so used this

    			if($image_tmp->isValid()){
    				$extension = $image_tmp->getClientOriginalExtension();
    				$filename = rand(111,99999).'.'.$extension;
    				$image_path = 'images/backend_images/blog/'.$filename;
    				Image::make($image_tmp)->save($image_path);
    			}
            }else if(!empty($data['current_image'])){
    			$filename = $data['current_image'];
            }else{
                 $filename = '';
			}

			if(empty($data['name'])){
    			$data['name'] = '';
    		}

			if(empty($data['content'])){
    			$data['content'] = '';
    		}

			if(empty($data['blog_date'])){
    			$data['blog_date'] = '';
    		}

    		Blog::where(['id'=>$id])->update(['name'=>$data['name'],'content'=>$data['content'],'blog_date'=>$data['blog_date'],'image'=>$filename]);
    		return redirect()->back()->with('flash_message_success','Blog updated Successfully.');
    	}

    	$blogDetail = Blog::where('id',$id)->first();
    	return view('admin.blog.edit_blog')->with(compact('blogDetail'));
    }

    public function viewBlogs(){
    	$blogAll = Blog::orderBy('id','DESC')->get();
    	// echo "<pre>"; print_r($blogAll); die;
		return view('admin.blog.view_blogs')->with(compact('blogAll'));

	}

	public function viewBlogs1(){
		$blogs = Blog::orderBy('id','DESC')->paginate(1);
		// echo "<pre>"; print_r($blogAll); die;
		return view('index')->with(compact('blogs'));

	}
	public function viewBlogs2($id){
		$blog = Blog::find($id);
		// echo "<pre>"; print_r($blogAll); die;
		return view('blog1',['blog'=>$blog]);

	}

	public function viewBlogsFront($id){
		$blog = Blog::where('id',$id)->orderBy('id','DESC')->first();
		// dd($blog);
		$comments = Comments::where('blog_id',$id)->orderBy('id','DESC')->get();
// dd($comments);
		return view('blog-details')->with(compact('blog', 'comments'));

	}






    public function deleteBlog(Request $request, $id=null){
    	Blog::where('id',$id)->delete();
    	return redirect()->back()->with('flash_message_success','Blog Deleted Successfully.');
    }

    public function allBlog(){
        $blogAll = Blog::orderBy('id','DESC')->get();
        // echo "<pre>"; print_r($blogAll); die;
        return view('blog')->with(compact('blogAll'));
    }

    public function blogDetail(Request $request, $id=null){
        $blogDetail = Blog::where(['id'=>$id])->first();
        // echo "<pre>"; print_r($blogDetail); die;
		// $comments = Comments::orderBy('id','DESC')->get();
			// dd($comments);
        return view('blog_detail')->with(compact('blogDetail','comments'));
	}





	public function comments(Request $request){
    	if($request->isMethod('post')){
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;

			$comments = new Comments;
			$comments->blog_id = $data['blog_id'];
    		$comments->name = $data['name'];
    		$comments->email = $data['email'];
			$comments->comment = $data['comment'];



    		$comments->save();
    		return redirect()->back()->with('flash_message_success','Comment Added Successfully.');
		}
		Comments::where(['id'=>$id])->update(['blog_id'=>$data['blog_id'],'name'=>$data['name'],'email'=>$data['email'],'comment'=>$data['comment']])->count;

		return view('blog-details');



	}


	public function likes(Request $request){
    	if($request->isMethod('post')){
    		$data = $request->all();
    		// echo "<pre>"; print_r($data); die;

			$likes = new Like;
			$likes->blog_id = $data['blog_id'];
    		$likes->ip_address = $data['ip_address'];
			$likes->save();

      		return redirect()->back()->with('flash_message_success','Like Added Successfully.');
        }		
        Like::where(['id'=>$id])->update(['blog_id'=>$data['blog_id'], 'ip_address'=>$data['ip_address']])->count;
 		return view('index');
	}

}
