<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Image;
use Session;
use Mail;

class ContactController extends Controller
{
	public function contact(Request $request){
		if($request->isMethod('post')){
            $data = $request->all();

            $messageData = [
                'name' => $data['name'],
                'email' => $data['email'],
                'subject' => $data['subject']
            ];

            $email = env('MAIL_USERNAME');
            Mail::send('emails.enquiry',$messageData,function($message) use($email){

                $message->to($email)->subject('Enquiry Mail - Zions Touch');


 
                });

            return redirect()->back()->with('flash_message_success','Contact Form Submitted Successfully.');
        }
        return view('contact');
    }
}
 