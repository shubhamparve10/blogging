<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use User;
use Mail;

class UserController extends Controller
{
    public function forgotPassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            // echo "<pre>"; print_r($data); die;
            $userCount = User::where('email',$data['email'])->count();
            if($userCount==0){
                return redirect()->back()->with('flash_message_error','We can not find a user with that Email address.');
            }

            //get User Details
            $userDetails = User::where('email',$data['email'])->first();
            //generate random password
            $random_password = Str::random(8);
            $new_password = bcrypt($random_password);

            //update pwd
            User::where('email',$data['email'])->update(['password'=>$new_password]);

            //send forgot email password email code
            $email = $data['email'];
            $name = $userDetails->name;
            $messageData = [
                'email'=>$email,
                'name'=>$name,
                'password'=>$random_password
            ];
            Mail::send('emails.forgotpassword',$messageData,function($message) use($email){
                $message->to($email)->subject('New Password - Zionstouch');
            });

            return redirect()->back()->with('flash_message_success','Password sent on your email, kindly check your Email.');
        }
        return view('users.forgot_password');
    }
}
