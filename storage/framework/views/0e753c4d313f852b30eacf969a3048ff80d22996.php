
    
<?php $__env->startSection('content'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo e(('/')); ?>">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="small-box bg-info">
              <div class="inner">
                <h3><i class="fas fa-sliders-h" aria-hidden="true"></i></h3>

                <p><h1>Dashboard</h1></p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders" aria-hidden="true"></i>
              </div>
             </a>  
            </div>
          </div>
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="small-box bg-success">
              <div class="inner">
                <h3><i class="fas fa fa-comment" aria-hidden="true"></i></h3>

                <p><h1>Blogs</h1></p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders" aria-hidden="true"></i>
              </div>
             </a>  
            </div>
          </div>
          <div class="col-lg-2 col-6">
            <!-- small box -->
            <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><i class="fas fa-sliders-h" aria-hidden="true"></i></h3>

                <p><h1>Video</h1></p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders" aria-hidden="true"></i>
              </div>
             </a>  
            </div>
          </div>  

          <div class="col-lg-2 col-6">
            
            <!-- small box -->
            <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><i class="fas fa-sliders-h" aria-hidden="true"></i></h3>

                <p><h1>Placements</h1></p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders" aria-hidden="true"></i>
              </div>
             </a>  
            </div>
          </div>  

 
          <!-- ./col -->
        </div>
         
             
              </div></div>
              <!-- /.card-footer -->
          
            <!-- /.card -->
 </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix\resources\views/admin/adminindex.blade.php ENDPATH**/ ?>