<?php $__env->startSection('content'); ?>

<!-- Banner Section -->
        <section class="page-banner">
            <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
            <div class="shape-1"></div>
            <div class="shape-2"></div>
            <div class="banner-inner">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <h1>Advanced Program In Digital Marketing</h1>
                        <div class="page-nav">
                            <ul class="bread-crumb clearfix">
                                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                <li class="active">Advanced Program In Digital Marketing</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
        </section>
        <!--End Banner Section -->
<section>
      
<div class="sidebar-page-container">
            <div class="auto-container">
                
                <div class="sidebar-title">
                    <div class="sec-title">
                             <h2>Full-Stack digital marketing program</h2>
                    </div>                                   
                </div>
                        <p class="mon">Before settling down for a course, get a teaching experience from our trainers.Enrol a seat for a free demo session of Full-stack digital marketing program now.</p>
                        <p class="mon">Contact Or Whatsapp <span class="fab fa-whatsapp"></span> Us: <button type="button" class="btn btn-primary active mr-2"><span class="icon flaticon-call" ></span>&nbsp;&nbsp;&nbsp;9284960102</button><button type="button" class="btn btn-primary active"> <span class="icon flaticon-call" ></span>&nbsp;&nbsp;&nbsp; 9922490943</button> </p>
                            
                            <!-- <li><span class="icon flaticon-call"></span><a href="tel:9284960102">9284960102</a></li> -->
                            
                        
                <div class="row clearfix">

                    <!--Content Side-->
                    <div class="content-side col-lg-4 col-md-12 col-sm-12">

                        <div class="team-block  service-details">
                            <div class="inner-box">
                                <h3>About our trainers</h3>
                                <div class="image-box">
                                    <img src="<?php echo e(asset('images/frontend_images/resource/team-1.jpg')); ?>" alt="" style="height: 200px; object-fit: contain;">                              
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="content-side col-lg-8 col-md-12 col-sm-12 mt-5">
                        <div class="lower-box" style="padding: 0px;">
                            <h5>Govind Chandak</h5>
                            <div class="designation"><h7 style="font-weight: 500;">Founder – Matrix Digital Technologies</h7></div>
                        </div>
                        <div class="text-content">                                
                            <p class="mon">Govind Chandak believes in inspiring and motivating while educating business, tools and technologies of digital marketing to make the trained students stand out in the crowd and reach millennial and generation consumers. 
                          </p>
                        </div>
                    </div>


                    <!--Sidebar Side-->
        <div class="sidebar-side col-lg-12  col-md-12 col-sm-12">
            <aside class="sidebar blog-sidebar">
                    <div class="sec-title centered">
                        <h3>BOOK MY SLOT</h3>
                    </div>
                    <div class="sidebar-widget call-up">
                         <div class="widget-inner">
                                    <div class="sidebar-title centered">
                                        <h4>Webinar Register form for Digital marketing Demo-Nagpur branch</h4>
                                    </div>
                            <div class="form-box">
                     
                                 <div class="default-form">
                                    <form method="post" action="http://layerdrops.com/linoorhtml/sendemail.php" id="contact-form">
                                        <div class="row clearfix">
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="text" name="name" value="" placeholder="Name" required="">
                                                </div>
                                            </div>
                                            <!-- <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="text" name="lastname" value="" placeholder="Last Name" required="">
                                                </div>
                                            </div> -->
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="text" name="phone" value="" placeholder="Phone Number" required="">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="email" name="email" value="" placeholder="Email Address"
                                                        required="">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="text" name="city" value="" placeholder="City Name"
                                                        required="">
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group col-lg-12 col-md-12 col-sm-12 centered">
                                                <button class="theme-btn btn-style-one ">
                                                    <i class="btn-curve"></i>
                                                    <span class="btn-title">submit</span>
                                                </button>
                                            </div>
                                        </div>
                                      </form>
                                  </div>
                            </div> 
                                    
                                </div>
            </div>
            </div>


                        </aside>
                    </div>

              

                  
                    <div class="content-side col-lg-12 col-md-12 col-sm-12">
                        <div class="service-details">
                            <div class="text-content">
                                <h3>What makes us stand out?</h3>
                                  <p class="mon">
                                    <i class="fa fa-check" aria-hidden="true"></i>   Digital marketing classes are conducted with a maximum of 6 students in a batch
                                        ensuring personal attention to every candidate.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   100% placement assurance.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   13+ Certificates.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Mock interviews are conducted to crack the interviews in the toughest 8companies.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Lifetime LMS access – Our recorded sessions of the digital marketing course are   
                                        present in LMS.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Weekly 1:1 mentoring session to understand the individual goals of every student to 
                                        provide them with the righteous path.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Trainers are from the high position of the digital marketing industry.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Guest lectures from the thought leaders.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   24*7 Facebook Community Support.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Career transition program by standard school Alumni Faculty.
                                    <i class="fa fa-check" aria-hidden="true"></i>   Global standard curriculum.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Access to all the webinars and boot camps.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Access to 50+ tools.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Hands on practise on 27+ modules, live projects, 25 assignments and 6 quizzes.

                                    </p>
                            </div><br>
                        </div>
                    </div>
                    <div class="content-side col-lg-12 col-md-12 col-sm-12">
                        <div class="service-details">
                            <div class="text-content">
                                <h3>Is this for me?</h3>
                                    <p  class="mon">
                                        Our digital marketing course is conducted from the beginning level, hence anyone who is passionate about online market can take the digital marketing training.
                                    </p>
                                    <p  class="mon">
                                    <i class="fa fa-check" aria-hidden="true"></i>   Graduates<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Post Graduates<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Students<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Management Students<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Working Professionals<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Start Up Founders<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Entrepreneurs<br>
                                    
                                    </p>
                            </div>
                        </div>
                    </div> 
                    

                </div>
            </div>
        </div>
</section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/webinar1.blade.php ENDPATH**/ ?>