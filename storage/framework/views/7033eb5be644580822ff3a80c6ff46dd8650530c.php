<?php $__env->startSection('content'); ?>

<!-- Banner Section -->
        <section class="page-banner">
            <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
            <div class="shape-1"></div>
            <div class="shape-2"></div>
            <div class="banner-inner">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <h1>Placements</h1>
                        <div class="page-nav">
                            <ul class="bread-crumb clearfix">
                                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                <li class="active">Placements</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Banner Section -->
<!-- Team Section -->
        <section class="team-section">
            <div class="auto-container">
                <div class="sec-title">
                     <h2>Our proud array of students who were placed</h2> 
                </div>
            <div class="row clearfix">
            <?php $__currentLoopData = $placements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $placement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <!--Team-->
                    <div class="team-block col-lg-2 col-md-6 col-sm-12">
                        <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="image-box">
                                <img class="imgsize" src="<?php echo e(asset('/images/backend_images/placement/'.$placement->image)); ?>" alt="">
                                
                            </div>
                            <div class="lower-box">
                                <h5><?php echo e($placement->name); ?> </h5>
                                <div class="designation"><?php echo e($placement->cname); ?></div>
                               <div class="designation"><?php echo e($placement->role); ?></div>   

                            </div>
                        </div>
                    </div>
 
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            
            </div>
        </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/placement.blade.php ENDPATH**/ ?>