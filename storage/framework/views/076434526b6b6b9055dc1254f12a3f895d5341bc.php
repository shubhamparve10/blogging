<?php $__env->startSection('content'); ?>

 <!-- Banner Section -->
        <section class="page-banner">
            <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
            <div class="shape-1"></div>
            <div class="shape-2"></div>
            <div class="banner-inner">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <h1>How a Digital marketer can do freelancing?</h1>
                        <div class="page-nav">
                            <ul class="bread-crumb clearfix">
                                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                <li class="active">How a Digital marketer can do freelancing?</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Banner Section -->

        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">
                     <!--Content Side-->
                    <div class="content-side col-lg-12 col-md-12 col-sm-12">
                        <div class="service-details">
                            <!--<div class="main-image image">-->
                            <!--    <img src="<?php echo e(asset('images/frontend_images/resource/featured-image-16.jpg')); ?>" alt="">-->
                            <!--</div>-->
                            <p class="mon">You're an educated digital marketer and want to get started with your agency but you do not have the vision to do so? Don't worry, we can be of your aid. Presenting a detailed description of how a digital marketer can do freelancing? </p>
                            <div class="text-content">
                                <h4>The first step - Look for your clients : </h4>
                                <p class="mon">There are many small businesses available who do not have a social media presence or a website, search for them. You can come across people who are on one of the social media channels and when you do thorough research you do not find their presence on other social media channels or their website would be missing. Few brands have impressive websites however, when you check their presence on social media is missing. You can find such business in your friend circle, neighbourhood and relatives. Make a list of all such small businesses.</p>
    
                            </div>
                            
                            <div class="text-content">
                                <h4>The second step - Make a plan :: </h4>
                                <p class="mon"> As a newcomer, you are required to make an entire plan on how you can be a valuable source for that brand. Note down their incomplete points to do marketing with the advantages of completing those points. If a person has a presence on only one channel of social media then explain about the advantages of having the presence on other social media channels. You would come across to the people having a social media presence and a website also, however, they would be doing mistakes while promoting their products, you can pen down a plan to improvise their work.</p>
                            </div>
                                
                            <div class="text-content">
                                <h4>The third step - Make dials :</h4>
                                <p class="mon">After doing research and making plans you are required to start dialling your prospects. Not everyone would answer your call, not everyone who attends your call would listen to your whole plan to improvise their business, not everyone who listens to your entire plan would be convinced but you do not need to get be demotivated by it because only a few people would agree to meet and the best part is rest of your plans is, those can be taken as case studies as it is not now or never, those case studies can come to your use in future, if not fully then partially. What matters the most are your efforts and strategies! Before meeting your client make a detailed plan to present in a PowerPoint presentation or a word document. In this field your word of mouth is your game plan, hence, choose wiser words and have a clear explanatory discussion with your clients. </p>
                            </div>
                              
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix\resources\views/blog3.blade.php ENDPATH**/ ?>