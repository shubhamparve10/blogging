
    
    <?php $__env->startSection('content'); ?>
    
    <div class="content-wrapper">
         <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-12">
                <h1 class="m-0">Placement Section</h1>
                <?php if(Session::has('flash_message_error')): ?>
              <div class="alert alert-error alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong><?php echo session('flash_message_error'); ?></strong>
              </div>
          <?php endif; ?>   
          <?php if(Session::has('flash_message_success')): ?>
              <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong><?php echo session('flash_message_success'); ?></strong>
              </div>
          <?php endif; ?>   
              </div> 
            </div> 
          </div> 
        </div>
        <!-- /.content-header -->
    
        <section class="content">
                  <div class="row">
                    <div class="col-md-9">
                    <form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo e(url('/admin/edit-placement/'.$placementDetail->id)); ?>" name="edit_banner" id="edit_banner" novalidate="novalidate">
                        <?php echo e(csrf_field()); ?>

                      <div class="card card-primary">
                          <div class="card-body">
                          <div class="form-group">
                            <label for="inputName"> Name</label>
                            <input type="text"   name="name" id="name" value="<?php echo e($placementDetail->name); ?>"  class="form-control" required>
                          </div>

                          <div class="form-group">
                            <label for="inputName"> Company Name</label>
                            <input type="text"   name="cname" id="cname" value="<?php echo e($placementDetail->cname); ?>"  class="form-control" required>
                          </div>

                          <div class="form-group">
                            <label for="inputName">Role </label>
                            <input type="text"   name="role" id="role" value="<?php echo e($placementDetail->role); ?>"  class="form-control" required>
                          </div>

                          <div class="form-group">
                <label class="control-label">Image :</label>
                <div class="controls">
                  <input name="image" id="image" type="file"class="form-control" size="19" style="opacity: 0;">

                  <?php if(!empty($placementDetail->image)): ?>
                    <input type="hidden" name="current_image" value="<?php echo e($placementDetail->image); ?>"> 
                  <?php endif; ?>

                  <?php if(!empty($placementDetail->image)): ?>
                  <img style="height: 50px;" src="<?php echo e(asset('images/backend_images/placement/'.$placementDetail->image)); ?>">
                  <?php endif; ?>

                </div>

                
              </div>
 

                          <!-- <div class="form-group">
                        <label for="inputDescription">Content</label>
                         <textarea class="summernote form-control"  id="inputDescription"name="content" required> <?php echo e($placementDetail->content); ?></textarea>
                      </div>
                           -->
                          <div class="form-group">
                            <label for="inputDate">Date</label>
                            <input type="date" name="placement_date" id="placement_date" value="<?php echo e($placementDetail->placement_date); ?>" class="form-control"required>
                          </div>
                          
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                    </div>
                    
                  </div>
                  <div class="row">
                    <div class="col-9">
                      <a href="#" class="btn btn-secondary">Cancel</a>
                      <input type="submit"  value="Update Placement" class="btn btn-success float-right">
            </form>
                    </div>
                  </div>  
        </section>
    
        </div>
        <br>
    
    
     
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/admin/placement/edit_placement.blade.php ENDPATH**/ ?>