<!-- Main Footer -->
<footer class="main-footer">
    <div class="auto-container">
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">

                <!--Column-->
                <div class="column col-xl-3 col-lg-6 col-md-6 col-sm-12">
                    <div class="footer-widget logo-widget">
                        <div class="widget-content">
                            <div class="logo">
                                <a href="<?php echo e(url('/')); ?>"><img id="fLogo" src="<?php echo e(asset ('images/frontend_images/matrix-white.png')); ?>" alt="" /></a>
                            </div>
                            <div class="text">Our institute in Nagpur &amp; Bengaluru strive to show a path of success to our students by making them understand and learn every concept at our classroom</div>
                            <ul class="social-links clearfix">
                                <li><a href="https://www.facebook.com/govind.chandak.50"><span class="fab fa-facebook-square"></span></a></li>
                                <li><a href="https://www.youtube.com/channel/UC_0Yew6BJoD4AlnlQXc8a5Q"><span class="fab fa-youtube"></span></a></li>
                                <li><a href="https://www.instagram.com/govind_chandak2012/"><span class="fab fa-instagram"></span></a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>

                <!--Column-->
                <div class="column col-xl-3 col-lg-6 col-md-6 col-sm-12">
                    <div class="footer-widget links-widget">
                        <div class="widget-content">
                            <h6>Quick Links</h6>
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-12">
                                    <ul>
                                        <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                        <li><a href="<?php echo e(url('about/')); ?>">About Us</a></li>
                                        <li><a href="<?php echo e(url('courses/')); ?>">Courses</a></li>                                     
                                        <li><a href="<?php echo e(url('blog/')); ?>">Knowledge Library</a></li>
                                        </ul>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <ul>
                                        <li><a href="<?php echo e(url('placement/')); ?>">Placement</a></li>
                                        <li><a href="<?php echo e(url('webinar/')); ?>">Upcoming Webinar</a></li>
                                        <li><a href="<?php echo e(url('contact')); ?>">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Column-->
                <div class="column col-xl-3 col-lg-6 col-md-6 col-sm-12">
                    <div class="footer-widget info-widget">
                        <div class="widget-content">
                            <h6>Contact</h6>
                            <ul class="contact-info">
                                <li class="address"><span class="icon flaticon-pin-1"></span>3rd Floor,Sadoday Arcade,<br>Western High Court Road, Dharampeth, Nagpur</li>
                                <li><span class="icon flaticon-call"></span><a href="tel:9284960102">9284960102</a></li>
                                <li><span class="icon flaticon-email-2"></span><a href="mailto:govindc0@gmail.com" target="_blank">govindc0@gmail.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--Column-->
                <div class="column col-xl-3 col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget newsletter-widget">
                                <div class="widget-content">
                                    <h6>Newsletter</h6>
                                    <div class="newsletter-form">
                                        <form method="post" action="contact.html">
                                            <div class="form-group clearfix">
                                                <input type="email" name="email" value="" placeholder="Email Address" required="">
                                                <button type="submit" class="theme-btn"><span class="fa fa-envelope"></span></button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="text">Sign up for our latest news &amp; articles. We won’t give you spam
                                        mails.</div>
                                </div>
                            </div>
                        </div>

                

            </div>

        </div>
    </div>

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="auto-container">
            <div class="inner clearfix">
                <div class="copyright">&copy; copyright 2020 by Digital Matrix  Technologies.Design by <a href="http://www.ycstech.in" target="_blank" class="f-link" style="color: #fff;">YCS Techsoft Pvt. Ltd.</a></div>
            </div>
        </div>
    </div>

</footer><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/layouts/frontLayout/front_footer.blade.php ENDPATH**/ ?>