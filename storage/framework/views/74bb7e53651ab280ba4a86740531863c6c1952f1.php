
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>
.view {
    margin: 0 auto; /* Added */
    float: none; /* Added */
    margin-bottom: 10px; /* Added */
    top:30px;
    width:40%;
}</style>
<?php $__env->startSection('content'); ?>
 
<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Blog Section</h1>
            <?php if(Session::has('flash_message_error')): ?>
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_error'); ?></strong>
          </div>
      <?php endif; ?>   
      <?php if(Session::has('flash_message_success')): ?>
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_success'); ?></strong>
          </div>
      <?php endif; ?>   
          </div> 
        </div> 
      </div> 
    </div>   


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"> All Enquiries/Feedback  </h3>
              

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile No.</th>
                  <th>Subject</th>
                  <th>Message</th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php $__currentLoopData = $allEnquiries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $enquiry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="gradeX">
                  <td><?php echo e($loop->index+1); ?></td>
                  <!-- <td style="text-align:center;"><span style="display: none;"><?php echo e($loop->index+1); ?></span><?php echo e($enquiry->id); ?></td> -->
                  <td style="text-align:center;"><?php echo e($enquiry->name); ?></td>
                  <td style="text-align:center;"><?php echo e($enquiry->email); ?></td>
                  <td style="text-align:center;"><?php echo e($enquiry->mobile); ?></td>
                  <td><?php echo e($enquiry->subject); ?></td>
                  <td><?php echo e($enquiry->comment); ?></td>
                  <td class="center " style="text-align: center !important; width: 25%;">
                    <a href="#myModal<?php echo e($enquiry->id); ?>" data-toggle="modal" class="btn btn-success btn-mini" title="View Blog">View</a>
            
                    <a  href="<?php echo e(url('admin/delete-enquiry/'.$enquiry->id)); ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-mini delete-confirm" title="Delete product">Delete</a>
                 </td>
                </tr>
                <div id="myModal<?php echo e($enquiry->id); ?>" class="modal hide">
                
                    <div class="view" style="width: 70rem;" id="myModal<?php echo e($enquiry->id); ?>">
                      <div class="card-body " style="background: beige;">
                      <button data-dismiss="modal" class="close" type="button">×</button>

                      <b>Name : </b>    <h3><?php echo e($enquiry->name); ?>  </h3><br>
                      <b>Email : </b>    <h3><?php echo e($enquiry->email); ?>  </h3><br>
                      <b>Mobile : </b>    <h3><?php echo e($enquiry->mobile); ?>  </h3><br>
                      <b>Subject : </b>    <h3><?php echo e($enquiry->subject); ?>  </h3><br>
                      <b>Comment : </b>    <h3><?php echo e($enquiry->comment); ?>  </h3><br>

                       
                      </div>
                     
                  </div>
 
                </div>  
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>


<?php $__env->stopSection(); ?>


 









<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/admin/contact/view_enquiries.blade.php ENDPATH**/ ?>