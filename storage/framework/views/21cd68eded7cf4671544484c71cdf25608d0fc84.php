<?php $__env->startSection('content'); ?>

        <!-- Banner Section -->
        <section class="page-banner">
            <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
            <div class="shape-1"></div>
            <div class="shape-2"></div>
            <div class="banner-inner">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <h1>Contact</h1>
                        <div class="page-nav">
                            <ul class="bread-crumb clearfix">
                                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                <li class="active">Contact</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Banner Section -->

        <!--Contact Section-->
     <section class="contact-section">
         <div class="auto-container">
                <div class="sec-title centered">
                    <h2>Get in touch with us!</h2>
                </div>

                <div class="upper-info">
                    <div class="row clearfix">
                        <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0ms"
                            data-wow-duration="1500ms">
                            <div class="inner-box">
                                <h5>location</h5>
                                <div class="text justify">
                                    <p  style=" overflow-wrap: break-word;">3rd Floor,Sadoday Arcade,Western High Court Road,Dharampeth,<br>Nagpur,Maharashtra.                                        
                                    </p>                                    
                                </div>
                            </div>
                        </div>

                        <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="300ms"
                            data-wow-duration="1500ms">
                            <div class="inner-box ">
                                <h5>call us</h5>
                                <div class="text">
                                    <ul class="info ">
                                        <li class="mon"><a href="tel:+91 99224 90943">+91 99224 90943</a></li>
                                        <li class="mon"><a href="tel:+91 90220 71935">+91 90220 71935</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms"
                            data-wow-duration="1500ms">
                            <div class="inner-box ">
                                <h5>mail us</h5>
                                <div class="text">
                                    <ul class="info">
                                        <li class="mon">Get Support Via email</li>
                                        <li class="mon"><a href="mailto:govindc0@gmail.com">govindc0@gmail.com</a></li>
                                        <!-- <li class="mon"style=" overflow-wrap: break-word;"><a href="www.matrixdigitaltechnologies.com">www.matrixdigitaltechnologies.com</a></li> -->
                                        

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms"
                            data-wow-duration="1500ms">
                            <div class="inner-box ">
                                <h5>Timing</h5>
                                <div class="text">
                                    <ul class="info">
                                        <li class="mon">Monday to Sunday    </li>
                                        <li class="mon">9:00 am – 9:00 pm</a></li>
                                        

                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="info-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="900ms"
                            data-wow-duration="1500ms">
                            <div class="inner-box">
                                <h5>baltimore</h5>
                                <div class="text">
                                    <ul class="info">
                                        <li>3 Lombabr 50 Baltimore</li>
                                        <li><a href="mailto:needhelp@linoor.com">needhelp@linoor.com</a></li>
                                        <li><a href="tel:666888000">666 888 000</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->

                  </div>
                </div>
            </div>
            <hr>        
         <div class="container-fluid">
             <div class="row clearfix">
                <div class="col-md-6">
                   <div class="form-box">
                     <div class="sec-title">
                        <h2>Get in touch</h2>
                     </div>
                     <div class="default-form">
                        <form action="<?php echo e(url('/contact')); ?>" method="post" id="contact-form">
                        <?php echo e(csrf_field()); ?>

                            <div class="row clearfix">
                                <div class="form-group col-lg-12 col-md-6 col-sm-12">
                                    <div class="field-inner">
                                        <input type="text" name="name" value="" placeholder="Your Name" required="">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-6 col-sm-12">
                                    <div class="field-inner">
                                        <input type="email" name="email" value="" placeholder="Email Address"
                                            required="">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-6 col-sm-12">
                                    <div class="field-inner">
                                        <input type="text" name="mobile" value="" placeholder="Phone Number" required="">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-6 col-sm-12">
                                    <div class="field-inner">
                                        <input type="text" name="subject" value="" placeholder="Occupation" required="">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <div class="field-inner">
                                        <textarea name="comment" placeholder="Write Message" required=""></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <button class="theme-btn btn-style-one">
                                        <i class="btn-curve"></i>
                                        <span class="btn-title">Send message</span>
                                    </button>
                                </div>
                            </div>
                          </form>
                      </div>
                  </div> 
               </div> 
                 
                  
                     <!--News Block-->
              <div class="col-md-6"style="width: 100%;height: 100%;">
                 <div class="sec-title centered">
                        <h2>Find Us On Map</h2>
                 </div>
                    <div class="map-box">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3721.2213859602525!2d79.05879031424732!3d21.14358618922583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bd4c05e26069321%3A0x8165961426be76f9!2sChaos%20Theory!5e0!3m2!1sen!2sin!4v1606305235351!5m2!1sen!2sin" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                     </div>                            
              </div>
                 
            </div>             
         </div>
   </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/contact.blade.php ENDPATH**/ ?>