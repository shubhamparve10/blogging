
    
<?php $__env->startSection('content'); ?>

<?php
    use App\Blog;
    use App\Video;
    use App\Placement;


    $blog = Blog::count();
    $video = Video::count();
    $placement = Placement::count();

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo e(('/')); ?>">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
<!-- <section class="content">
      <div class="container-fluid">
 
        <div class="row">
          <div class="col-lg-2 col-6">
            
            <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="small-box bg-info">
              <div class="inner">
                <h3><i class="fas fa-sliders-h" aria-hidden="true"></i></h3>

                <p><h1>Dashboard</h1></p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders" aria-hidden="true"></i>
              </div>
             </a>  
            </div>
          </div>
          <div class="col-lg-2 col-6">
  
            <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="small-box bg-success">
              <div class="inner">
                <h3><i class="fas fa fa-comment" aria-hidden="true"></i></h3>

                <p><h1>Blogs</h1></p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders" aria-hidden="true"></i>
              </div>
             </a>  
            </div>
          </div>
          <div class="col-lg-2 col-6">
          
            <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><i class="fas fa-sliders-h" aria-hidden="true"></i></h3>

                <p><h1>Video</h1></p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders" aria-hidden="true"></i>
              </div>
             </a>  
            </div>
          </div>  

          <div class="col-lg-2 col-6">
           
            <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><i class="fas fa-sliders-h" aria-hidden="true"></i></h3>

                <p><h1>Placements</h1></p>
              </div>
              <div class="icon">
              <i class="fa fa-sliders" aria-hidden="true"></i>
              </div>
             </a>  
            </div>
          </div>  

          </div>
          </div>
</section> -->




              <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
          <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Dashboard
             
                </span>
                <span class="info-box-number">
                   
                  <small></small>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </a>
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
          <a href="<?php echo e(('/admin/view-blogs')); ?>" class="small-box-footer">  
            <div class="info-box mb-3">

              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-comment"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Blogs
                
                <span class="badge badge-info right"><?php echo $blog ?> </span>
                </span>
                <span class="info-box-number"> </span>
              </div>
              <!-- /.info-box-content -->
            </div>
             
            <!-- /.info-box -->
          </div> </a>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
          <a href="<?php echo e(('/admin/view_video')); ?>" class="small-box-footer">  

            <!-- <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-video"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Videos
               
                <span class="badge badge-info right"><?php echo $video ?> </span>
                </span>
                <span class="info-box-number"> </span>
              </div>
             </div> -->
            <!-- /.info-box -->
          </div></a>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
          <a href="<?php echo e(('/admin/view-placements')); ?>" class="small-box-footer">  

            <!-- <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Placements
               
                <span class="badge badge-info right"><?php echo $placement ?> </span>
                </span>
                <span class="info-box-number"> </span>
              </div>
              
            </div> -->
          
          </div></a>
          <!-- /.col -->
        </div> </section>   
         
              </div>  
</div>
            
            
            
            
            
             
              <!-- /.card-footer -->
          
            <!-- /.card -->


            
 
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\zionweb\resources\views/admin/adminindex.blade.php ENDPATH**/ ?>