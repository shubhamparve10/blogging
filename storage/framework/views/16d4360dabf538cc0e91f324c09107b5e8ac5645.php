
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

          <style>
          .view {
              margin: 0 auto; /* Added */
              float: none; /* Added */
              margin-bottom: 10px; /* Added */
              top:30px;
              width:40%;
      }</style>
<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Blog Section</h1>
            <?php if(Session::has('flash_message_error')): ?>
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_error'); ?></strong>
          </div>
      <?php endif; ?>   
      <?php if(Session::has('flash_message_success')): ?>
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_success'); ?></strong>
          </div>
      <?php endif; ?>   
          </div> 
        </div> 
      </div> 
    </div>   

<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"> </h3>
                <a href="<?php echo e(url('/admin/add-blog')); ?>"><button style="float: right; margin: 3px 3px;" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Blog</button></a>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Image</th>
                  <th>Date</th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                <?php $__currentLoopData = $blogAll; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="gradeX">
                  <td><?php echo e($loop->index+1); ?></td>
                  <td style="text-align: center;width: 15%;"><?php echo e(Str::limit($blog->name, 20)); ?> </td>
                   <td style="text-align: center;width: 20%;"><?php echo Str::limit(strip_tags($blog->content),20); ?></td>
 

                  <td style="text-align: center;">
                    <?php if(!empty($blog->image)): ?>
                    <img src="<?php echo e(asset('/images/backend_images/blog/'.$blog->image)); ?>" style="height: 50px;">
                    <?php endif; ?>
                  </td>
                  <td style="text-align: center;"><?php echo e(date('d M Y', strtotime($blog->blog_date))); ?></td>

                  <td class="center " style="text-align: center !important; width: 25%;">
                    <a href="#myModal<?php echo e($blog->id); ?>" data-toggle="modal" class="btn btn-success btn-mini" title="View Blog">View</a>
                    <a href="<?php echo e(url('/admin/edit-blog/'.$blog->id)); ?>" class="btn btn-primary btn-mini" title="Edit Blog ">Edit</a>
                    <a  href="<?php echo e(url('/admin/delete-blog/'.$blog->id)); ?>" class="btn btn-danger btn-mini delete-confirm" title="Delete product">Delete</a>
                 </td>
                </tr>
                <div id="myModal<?php echo e($blog->id); ?>" class="modal hide">
                
                    <div class="view" style="width: 70rem;" id="myModal<?php echo e($blog->id); ?>">
                      <div class="card-body " style="background: beige;">
                      <button data-dismiss="modal" class="close" type="button">×</button>

                      <b>Name : </b>    <h3><?php echo e($blog->name); ?>  </h3><br>
                      <b>Date : </b>  <h3><?php echo $blog->blog_date; ?>  </h3><br>
                      <b>Content : </b>  <h3><?php echo $blog->content; ?> </h3><br>
                      <b>Image : </b>   <h3><center><img class="view"src="<?php echo e(asset('/images/backend_images/blog/'.$blog->image)); ?>"></center>  </h3><br>

                      </div>
                     
                  </div>
 
                </div>  
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>

    </div>
<script>
    $('.delete-confirm').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Are you sure you want to delete this Blog?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
</script>
<?php $__env->stopSection(); ?>
 


<!-- onclick="return confirm('Are you sure you want to delete this Blog?');" -->











 
<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\zionweb\resources\views/admin/blog/view_blogs.blade.php ENDPATH**/ ?>