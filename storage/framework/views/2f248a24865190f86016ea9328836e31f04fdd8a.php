
<?php $__env->startSection('content'); ?>

<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Edit Video</h1>
            <?php if(Session::has('flash_message_error')): ?>
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_error'); ?></strong>
          </div>
      <?php endif; ?>   
      <?php if(Session::has('flash_message_success')): ?>
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_success'); ?></strong>
          </div>
      <?php endif; ?>   
          </div> 
        </div> 
      </div> 
    </div>





    <section class="content col-md-10">
      <div class="container-fluid">
       <div class="card card-primary">
              <!-- <div class="card-header">
                <h3 class="card-title">Add Video</h3>
              </div> -->
              <!-- /.card-header -->
              <!-- form start -->
              <form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo e(url('/admin/add_video')); ?>" name="add_banner" id="add_banner" novalidate="novalidate"> 
              <?php echo e(csrf_field()); ?>

                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1"> Video Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="<?php echo e($videoDetails->title); ?>" placeholder="Title">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Add URL</label>
                    <input type="text" class="form-control" id="link" name="link" value="<?php echo e($videoDetails->link); ?>" placeholder="Add URL">
                  </div>
                 
                 
                <!-- /.card-body -->

                <div class="card-footer">
                  <button  type="submit" value="Update Banner" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
      </div>
      </div> 
</section>
</div> 
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/admin/video/edit_video.blade.php ENDPATH**/ ?>