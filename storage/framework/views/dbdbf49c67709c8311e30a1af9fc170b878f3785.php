<?php $__env->startSection('content'); ?>

 <!-- Banner Section -->
        <section class="page-banner">
            <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
            <div class="shape-1"></div>
            <div class="shape-2"></div>
            <div class="banner-inner">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <h1>How To Promote Your Brand On Social Media Platform?</h1>
                        <div class="page-nav">
                            <ul class="bread-crumb clearfix">
                                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                <li class="active">How To Promote Your Brand On Social Media Platform?</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Banner Section -->

        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">
                     <!--Content Side-->
                    <div class="content-side col-lg-12 col-md-12 col-sm-12">
                        <div class="service-details">
                            
                            <?php echo $blog->content; ?>

                                
                         
                                
                             
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/blog1.blade.php ENDPATH**/ ?>