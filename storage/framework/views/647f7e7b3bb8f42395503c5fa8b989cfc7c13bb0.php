<?php $__env->startSection('content'); ?>
<style>
    .img2{
        height:200px !important;
        object-fit: cover;
    }
</style>
<section class="page-banner">
    <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
    <div class="shape-1"></div>
    <div class="shape-2"></div>
    <div class="banner-inner">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <h1>About</h1>
                <div class="page-nav">
                    <ul class="bread-crumb clearfix">
                        <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="active">About</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Banner Section -->
<section class="faqs-section">
            <div class="auto-container">
                <div class="sec-title  ">
                 <div class="sec-title">
                        <h2>Why You Should Learn At Matrix Digital Technologies?</h2>
                        <div class="lower-text">Our aim is to arm individuals with practically oriented digital skills of future</div>
                 </div>
                <div class="row clearfix">
                    <div class="faq-block col-lg-6 col-md-12 col-sm-12">
                        <ul class="accordion-box clearfix">
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Industry Recognised Certifications</div>
                                <div class="acc-content">
                                    <div class="content">
                                          <div class="text">
                                            <p class="mon">We help you with an industry recognised certifications from Google, Facebook, IBM, Hubspot, SEM Rush, Matrix & Start Up India</p>
                                    </div>
                                    </div>
                                </div>
                            </li>

                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Practical Oriented Learning Architecture</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">We provide a case study and practical oriented learning approach with 70% practical sessions and only 30% theoretical sessions</p>
                                    </div>
                                    </div>
                                </div>
                            </li>

                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Live Projects</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">We help you with capstone and industry projects at the end of each module
                                        </p>
                                    </div>
                                    </div>
                                </div>
                            </li>

                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Global Standard Curriculum</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">We continuously keep on enhancing our digital marketing course and data science course curriculum to meet the industry requirements</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Lifetime LMS Access</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">Candidates at our digital marketing institute or data science institute received the life time access to our learning management system software to so that they can revise any part of course anytime and anywhere</p>
                                        </div>
                                    </div>
                                </div>
                            </li>

                           

                        </ul>
                    </div>
                    <div class="faq-block col-lg-6 col-md-12 col-sm-12">
                        <ul class="accordion-box clearfix">
                             <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Trainers From Senior Positions In The Industry</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">All our trainers and mentors are either coming straight from senior positions in the industry or they are the founders of high growth start ups in India to help you with right industry and business learning’s</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">100% Placement Assistance</div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">We help our students with 100% placement assistance across India with high growth start-ups and MNC’s. 
                                        </p>
                                    </div>
                                    </div>
                                </div>
                            </li>
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Resume, Work Portfolio & LinkedIn Profile Building

                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">We help our candidates with resume/ work portfolio building with a complete assistance to optimize the LinkedIn profile.</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Mock Interviews To Crack Toughest Companies

                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">All our candidates are compulsorily exposed to multiple mock interviews to develop confidence and to crack companies with ease</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn">Back Up Sessions, Doubt Sessions & 1:1 Mentoring Sessions

                                </div>
                                <div class="acc-content">
                                    <div class="content">
                                        <div class="text"><p class="mon">We help you with a regular back up session, doubt sessions and 1:1 mentoring sessions to help you to fulfil your personals goals by learning digital marketing course or data science course</p>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            
             <div class="content-side col-lg-12 col-md-12 col-sm-12">
                        <div class="service-details">
                            <div class="text-content">
                                <h3>Is This A Right Fit For Me?</h3>
                                    <p  class="mon">
                                        <i class="fa fa-check" aria-hidden="true"></i> Yes, it is 100% beginner friendly<br> 
                                        <i class="fa fa-check" aria-hidden="true"></i> Yes, it is 100% for coders and  non- coders<br> 
                                        <i class="fa fa-check" aria-hidden="true"></i> Yes, if you like project & case study based learning approach<br>  
                                        <i class="fa fa-check" aria-hidden="true"></i> Yes, if you are full of energy & looking to become a top-notch digital marketer or data scientist or data analyst<br> 
                                        <i class="fa fa-check" aria-hidden="true"></i> Yes, we will give you a toolkit to get started so as to make your on-boarding very smooth<br> 
                                    </p>
                                    <p  class="mon">So, If You Are A Student, Working Professional, Entrepreneur Or A Start-Up Founder, We Have Got You Covered.</p>
                            </div>
                        </div>
                    </div>
        </section>

<!-- Team Section -->
<section class="team-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>Meet the expert team</h2>
        </div>
        <div class="row clearfix">
            <!--Team-->
            <div class="team-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="image-box">
                        <img src="<?php echo e(asset('images/frontend_images/resource/team-1.jpg')); ?>" style="height:370; width: 426;" alt="">
                        <ul class="social-links clearfix">
                            <!-- <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                            <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                            <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li> -->
                        </ul>
                    </div>
                    
                </div>
            </div>
            <div class="team-block col-lg-8 col-md-6 col-sm-12">
                    <div class="lower-box">
                        <h3>Govind Chandak</h3>
                        <h5>Founder – Matrix Digital Technologies</h3>                      
                    </div>         
                                                               
                        <div class="text-content">
                           <p class="mon">Govind Chandak Inspires, Motivates And Educates Businesses On How To everage Emerging Technologies And Digital Marketing To Stand Out From The Noise And Reach The Millennial And Generation Z Consumers.</p>
                           <p class="mon">He Has A Diverse Background Working For The Mep Department Of Shapoorji Pallonji & Co. Ltd,Then As A Business Analyst At A Booming Digital Marketing Agency Known To Be Merkle -Sokrati Which Has Helped Launch Digital And Influencer Strategies Across Industry And Geographies With The World's Most Iconic Brands Like Airtel, OLA, Titan, Tansing, Zomato, Myntra, Viu And So On.Being A Proud Digital Marketer, Govind Has Also Managed And Organized Big Fat Events in India And Awarded As The Student Of The Year MBA Programme 2018 By MIT, Pune. He Has Also Marked By Several Publications In Renowned Journals of World.</p>   
                        </div>
            </div>
        </div>
        <hr>
    <!--Team-->
      <div class="row clearfix">
         <div class="team-block col-lg-8 col-md-6 col-sm-12">
                  <div class="lower-box">
                        <h3>Mr. Ramkumar Dhanoriya<h3>
                        <h5>Data Science Mentor - Matrix Digital Technologies</h5> 
                        <!--<h3>Founder – Matrix Digital Technologies</h3>                     -->
                    </div>         
                                                               
                        <div class="text-content">
                         <p class="mon">As A Data Scientist, He Help Companies To Analyse And Automate Their Business Using The Cutting Edge AI, ML And Deep Learning Technologies. He Is MTech (Electronics Engineering {Neural Networks Specialist}), With Research Experience And Publications In Machine 
                                Learning And Deep Learning.</p>
                          <p class="mon" style="font-weight: bolder;">
                            <b>The Tools, Skills, Software And Languages That I Work On Are As Follows:</b>
                         </p>
                          <p class="mon">Python (numpy, Pandas, Matplotlib, Sci-kit Learn, Tensorflow, Pytorch), Deep Learning (Tensorflow, Torch,caffe, Theano, H2O, Keras) NLP (NLTK, Stanford Core NLP), Computer Vision (Opencv, Torch), Machine Learning, Reinforcement Learning (Openai Gym), AI (IBM Watson, Openai), 
                              IOT (Raspberry Pi3), MATLAB, Django, Django Rest Framework, Restful Apis, C Language, SQL, MYSQL, Mongodb, Statistical Analysis, Tableau, Powerbi, Pyspark.</p>

                        </div>
                
            </div>
            <div class="team-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="image-box">
                        <img src="<?php echo e(asset('images/frontend_images/resource/team-2.jpg')); ?>" style="height:370; width: 426;" alt="">
                        <ul class="social-links clearfix">
                            <!-- <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                            <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                            <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li> -->
                        </ul>
                    </div>
                    
                </div>
             </div>
        </div>
    <hr>        
        
        <div class="row clearfix">
            <!--Team-->
            <div class="team-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="image-box">
                        <img src="<?php echo e(asset('images/frontend_images/resource/omkar_joshi.jpg')); ?>" style="height:370; width: 426;" alt="">
                        <ul class="social-links clearfix">
                            <!-- <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                            <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                            <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li> -->
                        </ul>
                    </div>
                    
                </div>
            </div>
            <div class="team-block col-lg-8 col-md-6 col-sm-12">
                <div class="lower-box">
                    <h3>MR. Omkar Mohan Joshi</h3>
                    <h5>Data Science Mentor -Matrix Digital Technologies</h5>                     
                </div>         
                                                           
                <div class="text-content">
                  <p class="mon">He is the Co-Founder of tech startup Nsemble.ai. We at Nsemble.ai, helps Automotive and Manufacturing companies to automate their quality inspection process using cutting
                   edge technologies like Artificial Intelligence and Deep learning.<br>He is MS in Statistics, Data Science & Data Analytics Mentor - NSDM INDIA.</p>
                   <p class="mon" style="font-weight: bolder;"><b>Tools, Skills, Software And Languages That He Works On Are As Follows:</b></p>
                  <p class="mon">
                   Data Analysis, Machine Learning, Deep Learning, Computer Vision, Natural Language Processing | Languages - Python, R, Julia Framework - Tensorflow, Keras, PyTorch, Django, Fast API Databases - SQL, MySQL, PostgreSoftware - MS PowerBI, Tableau, Advance Excel
                  </p>
                </div>
           </div>
        </div>
    </div>


</section>



<!-- Gallery Section -->
        <section class="gallery-section">
            <div class="auto-container">
                <!--MixitUp Galery-->
                <div class="mixitup-gallery">
                    <div class="row">
                         <div class="col-md-5 sec-title ">
                            <h2>work showcase </h2>
                        </div>
                        <!--Filter-->
                        <div class="col-md-7 filters centered clearfix">
                            <ul class="filter-tabs filter-btns clearfix">
                                <li class="active filter" data-role="button" data-filter="all">All</li>
                                <li class="filter" data-role="button" data-filter=".seminar">seminar</li>
                                <li class="filter" data-role="button" data-filter=".workshop">workshop</li>
                                <li class="filter" data-role="button" data-filter=".winter">Winter 2019</li>
                                <li class="filter" data-role="button" data-filter=".smd">SMD 2019</li>
                            </ul>
                        </div>
                    </div>
                    <div class="filter-list row">
                        <!-- Gallery Item -->
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/1.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/1.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Ambedkar College Seminar</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/2.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/2.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Ambedkar College Seminar</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/3.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/3.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Ambedkar College Seminar</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/4.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/4.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>BD College Wardha</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/5.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/5.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>BD College Wardha</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/6.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/6.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>CPC College</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/7.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/7.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>CPC College</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/8.jpeg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/8.jpeg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Datta Meghe Institute of Engineering, Technology Research, Wardha</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/9.jpeg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/9.jpeg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Datta Meghe Institute of Engineering, Technology Research, Wardha</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/10.jpeg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/10.jpeg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Datta Meghe Institute of Management Studies, Nagpur</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/11.jpeg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/11.jpeg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Datta Meghe Institute of Management Studies, Nagpur</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/12.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/12.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Dhanwate National College</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/13.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/13.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Dhanwate National College</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/14.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/14.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>DMIMS Event Photos</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/15.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/15.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Dhanwate National College</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/16.jpeg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/16.jpeg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>GKP College, Nagpur</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/17.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/17.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>GNIT</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/18.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/18.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Green Heaven</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/19.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/19.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>KDK MBA College</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/20.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/20.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>KDK MBA College</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/20.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/21.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>KITS Collge</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/22.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/22.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>KITS Collge</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/23.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/23.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Priyadarshini IG -2 Photos</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/24.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/24.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Priyadarshini IG -2 Photos</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/26.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/26.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Priyadarshni IG - 1</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/24.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/27.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Priyadarshni IG - 1</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/28.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/28.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Raisoni BBA Nagpur</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/29.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/29.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Raisoni BBA Nagpur</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/30.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/30.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Sipna Amravati Photos</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/31.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/31.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Sipna Amravati Photos</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/32.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/32.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>St Vincent Palloti, Nagpur</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all seminar col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/seminar/33.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/seminar/33.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>St Vincent Palloti, Nagpur</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all workshop col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/workshop/1.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/workshop/1.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Workshop</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all workshop col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/workshop/2.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/workshop/2.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Workshop</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all workshop col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/workshop/3.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/workshop/3.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Workshop</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all workshop col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/workshop/4.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/workshop/4.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Workshop</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all workshop col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/workshop/5.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/workshop/5.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Workshop</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all workshop col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/workshop/6.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/workshop/6.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Workshop</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all winter col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/winter/1.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/winter/1.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Winter</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all winter col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/winter/2.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/winter/2.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Winter</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all winter col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/winter/3.jpeg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/winter/3.jpeg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Winter</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all winter col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/winter/4.jpeg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/winter/4.jpeg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Winter</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all winter col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/winter/5.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/winter/5.jpeg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>Winter</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all smd col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/smd/1.JPG" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/smd/1.JPG" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>SMD 2019</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all smd col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/smd/2.JPG" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/smd/2.JPG" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>SMD 2019</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all smd col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/smd/3.JPG" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/smd/3.JPG" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>SMD 2019</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all smd col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/smd/4.JPG" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/smd/4.JPG" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>SMD 2019</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all smd col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/smd/5.JPG" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/smd/5.JPG" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>SMD 2019</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all smd col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/smd/6.JPG" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/smd/6.JPG" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>SMD 2019</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all smd col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/smd/7.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/smd/7.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>SMD 2019</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gallery-item mix all smd col-lg-3 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <figure class="image"><img src="images/frontend_images/gallery/smd/8.jpg" alt="" class="img2"></figure>
                                <a href="images/frontend_images/gallery/smd/8.jpg" class="lightbox-image overlay-box"
                                    data-fancybox="gallery"></a>
                                <div class="cap-box">
                                    <div class="cap-inner">
                                        <div class="title">
                                            <h5>SMD 2019</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix\resources\views/about.blade.php ENDPATH**/ ?>