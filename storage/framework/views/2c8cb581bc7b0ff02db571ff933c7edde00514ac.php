<header class="main-header header-style-one">

    <!-- Header Upper -->
    <div class="header-upper">
        <div class="inner-container clearfix">
            <!--Logo-->
            <div class="logo-box">
                <div class="logo">
                    <a href="<?php echo e(url('/')); ?>" title="Matix digital Technologies">
                        <img src="<?php echo e(asset('images/frontend_images/matrix1.png')); ?>" id="thm-logo" alt="Matix digital Technologies"  title="Matix digital Technologies">
                    </a>
                </div>
            </div>
            <div class="nav-outer clearfix" >   
            <!--*style="background:#ffffff8f;"-->
                <!--Mobile Navigation Toggler-->
                <div class="mobile-nav-toggler"><span class="icon flaticon-menu-2"></span><span
                        class="txt">Menu</span></div>

                <!-- Main Menu -->
                <nav class="main-menu navbar-expand-md navbar-light">
                    <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                        <ul class="navigation clearfix">
                            <!-- <li><a href="<?php echo e(url('/')); ?>">Home</a>
                                <ul>
                                    <li><a href="index-2.html">Home Style 01</a></li>
                                    <li><a href="index-3.html">Home Style 02</a></li>
                                    <li><a href="index-4.html">Home Style 03</a></li>
                                    <li><a href="one-page.html">Home One Page <span>new</span></a></li>
                                </ul>
                            </li> -->
                            <li><a href="<?php echo e(url('about/')); ?>">About Us</a></li>
                           <!--  <li class="dropdown"><a href="team.html">Pages</a>
                                <ul>
                                    <li><a href="team.html">Our Team</a></li>
                                    <li><a href="testimonials.html">Testimonials</a></li>
                                    <li><a href="faqs.html">FAQs</a></li>
                                    <li><a href="not-found.html">404 Page</a></li>
                                </ul>
                            </li> -->
                            <li class="dropdown"><a href="<?php echo e(url('courses/')); ?>">Courses</a>
                                <ul>
                                    <li><a href="<?php echo e(url('digital_marketing/')); ?>">Advanced Program In Digital Marketing</a></li>
                                    <li class="dropdown"><a href="#">Advanced Program In Data Science With Python</a>
                                    <ul>
                                         <li><a href="<?php echo e(url('datascience_python_maharashtra/')); ?>">Maharashtra</a></li>
                                        <li><a href="<?php echo e(url('datascience_python_banglore/')); ?>">Banglore</a></li>
                                    </ul>
                                    </li>
                                    <li><a href="<?php echo e(url('data_analytics/')); ?>">Advanced Program In Data Analytics With Python</a>
                                    </li><!-- 
                                    <li><a href="seo.html">SEO & Content Writting</a></li>
                                    <li><a href="app-development.html">App Development</a></li>
                                    <li><a href="ui-designing.html">UI/UX Designing</a></li> -->
                                </ul>
                            </li>
                            
                            <li class="dropdown"><a>Knowledge Library</a>
                                <ul>
                                    <li><a href="<?php echo e(url('blog/')); ?>">Blogs</a></li>
                                    <li><a href="<?php echo e(url('videos/')); ?>">Videos</a></li>
                                    <li><a href="<?php echo e(url('gallery/')); ?>">gallery</a></li>

                                </ul>
                            </li>
                            <li><a href="<?php echo e(url('placement/')); ?>">Placements</a>


                            <li class="dropdown"><a href="<?php echo e(url('webinar/')); ?>">Upcoming Webinars</a>
                                <ul>
                                    <li><a href="<?php echo e(url('webinar1/')); ?>">Advanced Program In Digital Marketing</a>
                                    </li>
                                    <li><a href="<?php echo e(url('webinar2/')); ?>">Fundamentals Course On Advanced Data Science With Python</a></li>
                                </ul>
                            </li>


                            <li><a href="<?php echo e(url('contact/')); ?>">Contact</a></li>
                        </ul>
                    </div>
                </nav>
            </div>

            <div class="other-links clearfix">
                <!--Search Btn-->
           <!--  <div class="search-btn">
                <button type="button" class="theme-btn search-toggler"><span
                        class="flaticon-loupe"></span></button>
            </div>-->                
                <div class="link-box">
                    <div class="call-us">
                        <a class="link" href="tel:9284960102">
                            <span class="icon"></span>
                            <span class="sub-text">Call Anytime</span>
                            <span class="number">+91 9284960102</span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--End Header Upper-->

</header>
<!-- End Main Header -->


<!--Mobile Menu-->
<div class="side-menu__block">

    <div class="side-menu__block-overlay custom-cursor__overlay">
        <div class="cursor"></div>
        <div class="cursor-follower"></div>
    </div><!-- /.side-menu__block-overlay -->
    <div class="side-menu__block-inner ">
        <div class="side-menu__top justify-content-end">

                    <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?php echo e(('images/frontend_images/icons/close-1-1.png')); ?>" alt=""></a>
                </div><!-- /.side-menu__top -->


        <nav class="mobile-nav__container">
            <!-- content is loading via js -->
        </nav>
        <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
        <div class="side-menu__content">
           
            <p><a href="">928496010</a> <br> <a href="tel:928496010">+91 928496010</a></p>
            <div class="side-menu__social">
                <ul class="row clearfix">
                    <li><a href="https://www.facebook.com/govind.chandak.50"><span class="fab fa-facebook-square"></span>
                    </a></li>
                    <li><a href="https://www.youtube.com/channel/UC_0Yew6BJoD4AlnlQXc8a5Q"><span class="fab fa-youtube">
                    </span></a></li>
                    <li><a href="https://www.instagram.com/govind_chandak2012/"><span class="fab fa-instagram"></span></a>
                    </li>
                                
                 </ul>
            </div>
        </div><!-- /.side-menu__content -->
    </div><!-- /.side-menu__block-inner -->
</div><!-- /.side-menu__block -->

<!--Search Popup-->
<div class="search-popup">
    <div class="search-popup__overlay custom-cursor__overlay">
        <div class="cursor"></div>
        <div class="cursor-follower"></div>
    </div><!-- /.search-popup__overlay -->
    <div class="search-popup__inner">
        <form action="#" class="search-popup__form">
            <input type="text" name="search" placeholder="Type here to Search....">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div><!-- /.search-popup__inner -->
</div><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/layouts/frontLayout/front_header.blade.php ENDPATH**/ ?>