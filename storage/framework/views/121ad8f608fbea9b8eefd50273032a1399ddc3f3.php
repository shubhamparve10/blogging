<footer class="theme-footer">
				<div class="container">
					<div class="logo"><a href="index.html"><img src="images/logo/logo.png" alt=""></a></div>
					<p class="footer-text">Blog and Portfolio Theme</p>
					<ul class="social-icon">
						<li class="icon"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<!-- <li class="icon"><a href="#"><i class="fab fa-twitter"></i></a></li> -->
						<li class="icon"><a href="https://www.youtube.com/channel/UCA6qp76Pk_3gr92yqZUyCog"><i class="fab fa-youtube"></i></a></li>
						<li class="icon"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
					<p class="copyright">&copy; Copyright ©2020  - All Rights Reserved.</p>
				</div> <!-- /.container -->
			</footer> <!-- /.theme-footer -->

            <button class="scroll-top tran3s">
				<i class="fa fa-angle-up" aria-hidden="true"></i>
			</button>
		 <?php /**PATH C:\xampp\htdocs\zionstouch\resources\views/layouts/frontLayout/front_footer.blade.php ENDPATH**/ ?>