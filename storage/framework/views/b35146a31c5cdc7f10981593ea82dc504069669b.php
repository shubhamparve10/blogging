
<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('js/frontend_js/jquery.js')); ?>"></script>
<style>
		/* @media  screen and (min-width: 600px) {
		.m1{
	        position: absolute;
	        left: -158px;
	        top: -100px;
	        font-size: 40px;
	    }
	} */

	@media  screen and (min-width: 601px) {
		  .m1{
		    position: absolute;
		        left: 66px;
		        top: -100px;
		        font-size: 60px;
		  }
		  .theme-button-one1 {
		    line-height: 24px;
		    font-size: 9px;
		    padding: 0px 6px;
		    text-transform: uppercase;
		    letter-spacing: 0.8px;
		    color: #fff;
		    border-width: 1px;
			text-align: center;
			border-style: solid;
		    border-color: #fd032d;
			background-color:#fd032d;
		}
		 .theme-button-one1:hover {
			background: #fff;
			color:#fd032d;
		}
		}

		@media  screen and (max-width: 600px) {
		    .m1 {
		        position: absolute;
		    top: -40px;
		    left: 13px;
		    font-size: 33px;
		  }
		  }
		  .theme-button-one1 {
		    line-height: 24px;
		    font-size: 9px;
		    padding: 0px 6px;
		    text-transform: uppercase;
		    letter-spacing: 0.8px;
		    color: #fff;
		    border-width: 1px;
			text-align: center;
			border-style: solid;
		    border-color: #fd032d;
			background-color:#fd032d;
		}
		 .theme-button-one1:hover {
			background: #fff;
			color:#fd032d;
		}
	}
</style>
<?php
use App\Comments;
use App\Like;
?>
		<!--
			=============================================
				Theme Main Banner
			==============================================
			-->
			<div class="theme-banner-section-four section-margin-bottom">
				<div class="main-banner-slider-two">
					<div class="item">
						<div class="main-bg-wrapper" style="background:url(<?php echo e(url('images/home/banner.jpg')); ?>) no-repeat center center">
							<div class="overlay">
								<div class="wrapper">
									<div class="container m1">
										<h2 style="color:#fff; " class="red m1">If you want to fly,<br> give up the things<br> that weigh you down </h2>
									</div>
								</div>
							</div> <!-- /.overlay -->
						</div> <!-- /.main-bg-wrapper -->
					</div> <!-- /.item -->
					<div class="item">
						<div class="main-bg-wrapper" style="background:url(<?php echo e(url('images/home/banner.jpg')); ?>) no-repeat center center">
							<div class="overlay">
								<div class="wrapper">
									<div class="container m1">
										<h2 style="color:#fff; " class="red m1">If you want to fly,<br> give up the things<br> that weigh you down </h2>
									</div>
								</div>
							</div> <!-- /.overlay -->
						</div> <!-- /.main-bg-wrapper -->
					</div> <!-- /.item -->


				</div> <!-- /.main-banner-slider-two -->
			</div> <!-- /.theme-banner-sectiron-four -->

			<!--
			=============================================
				Main Blog Post Wrapper
			==============================================
			-->
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-12 blog-grid-style hover-effect-one">
						<div class="single-blog-post">
						<?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

						<a href="<?php echo e(url('/blog-details/'.$blog->id)); ?>"><div class="image-box"><img class="view"src="<?php echo e(asset('/images/backend_images/blog/'.$blog->image)); ?>"></div></a>
							<div class="post-meta-box bg-box">
								<ul class="author-meta clearfix">
									<!-- <li class="tag"><a href="catergories.html">Art</a></li> -->
									<li class="date"><a href="#"><?php echo e(date('d F Y | h:iA', strtotime($blog->blog_date))); ?></a></li>
								</ul>
								<h4 class="title"><a href="<?php echo e(url('/blog-details/'.$blog->id)); ?>"><?php echo e($blog->name); ?>   </a></h4>
								<ul class="share-meta clearfix">
								<?php
									$comments = Comments::where('blog_id',$blog->id)->count();
									$likecount = Like::where('blog_id',$blog->id)->count();
								?>
									<li><a href="<?php echo e(url('/blog-details/'.$blog->id)); ?>"><i class="icon flaticon-comment"></i>Comments ( <?php echo $comments ?> )</a></li>
									<?php $cnt = Like::where('blog_id',$blog->id)->where('ip_address',$_SERVER['REMOTE_ADDR'])->count(); ?>
									<?php if($cnt <= 0): ?>
									<li>



									<form action="<?php echo e(url('/likes')); ?>" method="post" id="likeButtonForm" name="likeButtonForm"><?php echo csrf_field(); ?>
										<input type="hidden" name="blog_id" value="<?php echo e($blog->id); ?>">
										<input type="hidden" name="ip_address" value="<?php echo e($_SERVER['REMOTE_ADDR']); ?>">
										<!-- <li><a href="#"><i class="icon flaticon-like-heart"></i>Likes (00)</a></li> -->
										<button style='background: transparent; color: #737373;text-transform: uppercase;font-weight: 600;font-size: 10px;letter-spacing: 1.5px;' type="submit"><i class="icon flaticon-like-heart"></i> Likes  ( <?php echo e($likecount); ?> )</button>
									</form>



									</li>
									<?php else: ?>
									<li><a ><i class="fa fa-heart" aria-hidden="true" style="color:red"></i> Likes  ( <?php echo $likecount ?> )</a></li>
									<!-- <button type="button" style='background: transparent;'><i class="fa fa-heart" aria-hidden="true" style="color:red"></i> Likes  ( <?php echo $likecount ?> )</button> -->

									<?php endif; ?>
									<li class="share-option">
										<button>  <div  class="addthis_inline_share_toolbox"></div></button>
										<ul class=" ">
										<!-- <div class="addthis_inline_share_toolbox"></div> -->
											<!-- <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
											<li><a href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
											<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li> -->

										</ul>
									</li> <!-- /.share-option -->
								</ul>
							<p><?php echo Str::limit(strip_tags($blog->content),305); ?> 	<a href="<?php echo e(url('/blog-details/'.$blog->id)); ?>"> <button class="theme-button-one1">Continue Reading </button> </p></a>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<!-- /.post-meta-box -->
						</div> <!-- /.single-blog-post -->

						<div class="theme-pagination text-center Page navigation ">
						<ul class="pagination justify-content-center">
									 <li> <?php echo e($blogs->links()); ?></li>
									 </ul>
						</div>


						<!-- /.theme-pagination -->
					</div> <!-- /.col- -->

					<!-- ======================== Theme Sidebar =============================== -->
					<div class="col-lg-4 col-md-7 col-12 theme-main-sidebar">
						<div class="sidebar-box bg-box about-me">
							<h6 class="sidebar-title">About me</h6>
							<img src="images/home/1.jpg" alt="">
							<p style="text-align:justify;">Hello, my name is Zoe. A qualified Artist, Tutor and Healthcare professional.
								A diverse Specialist with experience in Broadcasting, Teaching, Nursing, Acting and Directing.</p>
							<!-- <div class="clearfix"><img src="images/home/sign.png" alt="" class="signature float-right"></div> -->
						</div> <!-- /.about-me -->
						<!-- <div class="sidebar-box bg-box sidebar-categories">
							<h6 class="sidebar-title">Categories</h6>
							<ul>
								<li><a href="catergories.html">Web Design</a></li>
								<li><a href="catergories.html">Fashion</a></li>
								<li><a href="catergories.html">Graphic Design</a></li>
								<li><a href="catergories.html">News and Events</a></li>
								<li><a href="catergories.html">Personal</a></li>
								<li><a href="catergories.html">Branding Agency</a></li>
								<li><a href="catergories.html">Lifestyle</a></li>
								<li><a href="catergories.html">Travel</a></li>
							</ul>
						</div> -->
						 <!-- /.sidebar-categories -->
						<div class="sidebar-box bg-box sidebar-trending-post">
							<h6 class="sidebar-title">Popular Broadcasts</h6>
							<div class="single-trending-post clearfix">
								<img src="<?php echo e(url('images/home/th1.webp')); ?>" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="https://youtu.be/fBcUbxm7vcI" target="_blank">Love at Christmas (Part One) l Zion's Touch</a></h6>

								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->
							<div class="single-trending-post clearfix">
								<img src="<?php echo e(url('images/home/th4.webp')); ?>" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="https://youtu.be/1IiLwg58EmM"  target="_blank">Communicate! Love, Romance and Time l Zion's Touch</a></h6>

								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->
							<div class="single-trending-post clearfix">
								<img src="<?php echo e(url('images/home/th5.webp')); ?>" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="https://youtu.be/fmR0UR_rM_8"  target="_blank">Copy of Finally Monetized! Challenges I Overcame l Zion's Touch</a></h6>

								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->

						</div> <!-- /.sidebar-trending-post -->
						<!-- <div class="sidebar-box bg-box sidebar-categories">
							<h6 class="sidebar-title">Archives</h6>
							<ul>
								<li><a href="archives.html">May 2018</a></li>
								<li><a href="archives.html">April 2018</a></li>
								<li><a href="archives.html">March 2018</a></li>
								<li><a href="archives.html">February 2018</a></li>
								<li><a href="archives.html">January 2018</a></li>
								<li><a href="archives.html">December 2107</a></li>
								<li><a href="archives.html">November 2017</a></li>
								<li><a href="archives.html">October 2017</a></li>
							</ul>
						</div>  -->
						<!-- /.sidebar-categories -->
						<!-- <div class="sidebar-box bg-box sidebar-tags">
							<h6 class="sidebar-title">Keyword</h6>
							<ul class="clearfix">
								<li><a href="catergories.html">Design</a></li>
								<li><a href="catergories.html">Fashion</a></li>
								<li><a href="catergories.html">Graphic</a></li>
								<li><a href="catergories.html">News</a></li>
								<li><a href="catergories.html">Personal</a></li>
								<li><a href="catergories.html">Branding</a></li>
								<li><a href="catergories.html">Lifestyle</a></li>
								<li><a href="catergories.html">Travel</a></li>
							</ul>
						</div> -->
						 <!-- /.sidebar-tags -->
						<!-- <div class="sidebar-box bg-box sidebar-newsletter">
							<h6 class="sidebar-title">newsletter</h6>
							<form action="#">
								<input type="email" placeholder="Your email address" required>
								<button class="theme-button-one">Sign up</button>
							</form>
						</div> -->
						 <!-- /.sidebar-newsletter -->
					</div> <!-- /.theme-main-sidebar -->
				</div> <!-- /.row -->
			</div> <!-- /.container -->
			


			<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\zionweb\resources\views/index.blade.php ENDPATH**/ ?>