 

<?php $__env->startSection('content'); ?>
			<!-- ===================================================
				About Me Wrapper
			==================================================== -->
			<div class="contact-us">
				<div class="back-to-home"><a href="<?php echo e(('/')); ?>"><i class="flaticon-left-arrow"></i> Back to home</a></div>
				<div class="main-text-wrapper">
					<div class="container">
						<div class="contact-form">
							<h2>Contact</h2>
							<p>You can contact me almost about anything</p>
							<form action="inc/sendemail.php" class="form-validation" autocomplete="off">
								<label>full Name</label>
								<input type="text" placeholder="John Doe" name="name" autofocus>
								<label>Email address</label>
								<input type="email" placeholder="ex@example.com" name="email">
								<label>Your message</label>
								<textarea placeholder="Message" name="message"></textarea>
								<button class="theme-button-one">Submit</button>
							</form>
						</div> <!-- /.contact-form -->
					</div> <!-- /.container -->
				</div> <!-- /.main-text-wrapper -->
				<!--Contact Form Validation Markup -->
				<!-- Contact alert -->
				<div class="alert-wrapper" id="alert-success">
					<div id="success">
						<button class="closeAlert"><i class="fas fa-window-close"></i></button>
						<div class="wrapper">
			               	<p>Your message was sent successfully.</p>
			             </div>
			        </div>
			    </div> <!-- End of .alert_wrapper -->
			    <div class="alert-wrapper" id="alert-error">
			        <div id="error">
			           	<button class="closeAlert"><i class="fas fa-window-close"></i></button>
			           	<div class="wrapper">
			               	<p>Sorry!Something Went Wrong.</p>
			            </div>
			        </div>
			    </div> <!-- End of .alert_wrapper -->
			</div> <!-- /.contact-us -->



			<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\zionstouch\resources\views/contact.blade.php ENDPATH**/ ?>