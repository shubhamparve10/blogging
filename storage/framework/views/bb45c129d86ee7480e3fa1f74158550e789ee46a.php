<!DOCTYPE html>
<html lang="en">

<head>
		<meta charset="UTF-8">
		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- For Window Tab Color -->
		<!-- Chrome, Firefox OS and Opera -->
		<meta name="theme-color" content="#2c2c2c">
		<!-- Windows Phone -->
		<meta name="msapplication-navbutton-color" content="#2c2c2c">
		<!-- iOS Safari -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#2c2c2c">

		<title>Zion's Touch</title>

		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="images/fav-icon/ficon.png">


		<!-- Main style sheet -->
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/frontend_css/style.css')); ?>">
		<!-- responsive style sheet -->
		<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/frontend_css/responsive.css')); ?>">
		<!-- Theme-Color css -->
		<link rel="stylesheet" id="jssDefault" href="<?php echo e(asset('css/frontend_css/color.css')); ?>">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300&display=swap" rel="stylesheet">
		
		<!-- Fix Internet Explorer ______________________________________-->

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="mainstyle/html5shiv.js"></script>
			<script src="mainstyle/respond.js"></script>
		<![endif]-->

		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ff80d66d4855edc"></script>
        <style>
            .red{
                font-family: 'Raleway', sans-serif;
            }
        </style>
    </head>

<body>


		<div class="main-page-wrapper">
			<!-- ===================================================
				Loading Transition
			==================================================== -->
			<!-- <div id="loader-wrapper">
				<div id="loader">
	                <span></span>
	                <span></span>
	                <span></span>
	                <span></span>
	                <span></span>
	            </div>
			</div> -->


        <?php echo $__env->make('layouts.frontLayout.front_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->yieldContent('content'); ?>

        <?php echo $__env->make('layouts.frontLayout.front_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    </div>
    <!--End pagewrapper-->

    	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    	<!-- jQuery -->
		<script src="<?php echo e(asset('mainstyle/jquery.2.2.3.min.js')); ?>"></script>
		<!-- Popper js -->
		<script src="<?php echo e(asset('mainstyle/popper.js/popper.min.js')); ?>"></script>
		<!-- Bootstrap JS -->
		<script src="<?php echo e(asset('mainstyle/bootstrap/js/bootstrap.min.js')); ?>"></script>
		<!-- Style-switcher  -->
		<script src="<?php echo e(asset('mainstyle/jQuery.style.switcher.min.js')); ?>"></script>
		<!-- jquery-easy-ticker-master -->
		<script src="<?php echo e(asset('mainstyle/jquery-easy-ticker-master/jquery.easy-ticker.min.js')); ?>"></script>
		<!-- jquery easing -->
		<script src="<?php echo e(asset('mainstyle/jquery.easing.1.3.js')); ?>"></script>
		<!-- Font Awesome -->
		<script src="<?php echo e(asset('fonts/font-awesome/fontawesome-all.min.js')); ?>"></script>
		<!-- Master Slider -->
        <script src="<?php echo e(asset('mainstyle/masterslider/masterslider.min.js')); ?>"></script>
	    <!-- menu  -->
		<script src="<?php echo e(asset('mainstyle/menu/src/js/jquery.slimmenu.js')); ?>"></script>
		<!-- owl.carousel -->
		<script src="<?php echo e(asset('mainstyle/owl-carousel/owl.carousel.min.js')); ?>"></script>
		<!-- Fancybox -->
		<script src="<?php echo e(asset('mainstyle/fancybox/dist/jquery.fancybox.min.js')); ?>"></script>
		<!-- Youtube Video -->
		<script src="<?php echo e(asset('mainstyle/jquery.fitvids.js')); ?>"></script>

		<!-- Theme js -->
		<script src="<?php echo e(asset('js/frontend_js/theme.js')); ?>"></script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\zionweb\resources\views/layouts/frontLayout/front_design.blade.php ENDPATH**/ ?>