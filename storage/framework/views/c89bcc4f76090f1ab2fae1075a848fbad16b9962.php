	<!--
			=============================================
				Top Header
			==============================================
			-->
			<div class="top-header">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-12">
							<!-- <div class="news-bell">Recent Post :</div> -->
							<!-- <div class="breaking-news easyTicker">
								<div class="wrapper">
									<div class="list"><a href="blog-details-v1.html">One morning, when Gregor Samsa woke from troubled dreams.</a></div>
									<div class="list"><a href="blog-details-v1.html">CNN's Kyung Lah sits down with Japan's World Cup-winning.</a></div>
									<div class="list"><a href="blog-details-v1.html">Song Byeok had every reason to be pleased with his success.</a></div>
								</div>
                            </div>  -->
                            <!-- /.breaking-news -->
						</div>
						<div class="col-lg-4 col-12">
							<ul class="social-icon text-right">

								<li class="icon"><a href="#"><i class="fab fa-spotify"></i></a></li>
 								<li class="icon"><a href="https://www.youtube.com/channel/UCA6qp76Pk_3gr92yqZUyCog"><i class="fab fa-youtube"></i></a></li>
                                <li class="icon"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>

								<!-- <li class="search-button">
									<button class="search" id="search-button"><i class="fas fa-search"></i></button>
								</li> -->
							</ul>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.top-header -->



			<!--
			=============================================
				Theme Header
			==============================================
			-->






<header class="theme-main-header">
				<div class="container">
					<div class="content-holder clearfix">
						<!-- <div class="logo"><a href="<?php echo e(('/')); ?>"><img src="images/logo/logo.png"   alt=""></a></div> -->
						<div class="logo"><a href="<?php echo e(('/')); ?>"><img src="images/logo/logo.png"    alt=""></a></div>

						<!-- ============== Menu Warpper ================ -->
				   		<div class="menu-wrapper">
				   			<nav id="mega-menu-holder" class="clearfix">
							   <ul class="clearfix">

                               <li><a href="<?php echo e(('/')); ?>">Blogs</a></li>
                                  <li><a href="<?php echo e(('/about')); ?>">About</a></li>
                                  <li><a href="<?php echo e(('/contact')); ?>">Contact</a></li>

                                  <!-- <li class="social-icon" style="color:black" ><a href="#">Fb</a></li> -->
 							      <!-- <li class="active"><a href="#">Home</a>
							      	<ul class="dropdown">
							            <li class="active"><a href="index.html">Home Standard</a></li>
							            <li><a href="index-2.html">Home Clasic</a></li>
							            <li><a href="index-3.html">Home Video Blog</a></li>
							            <li><a href="index-4.html">Home fullwidth</a></li>
							            <li><a href="index-5.html">Home masonry</a></li>
							            <li><a href="index-6.html">Home Image blog</a></li>
							        </ul>
							      </li> -->
							      <!-- <li><a href="#">Features</a>
							      	<ul class="dropdown">
							      		<li><a href="blog-v1.html">Blog Right Sidebar</a></li>
							      		<li><a href="blog-v2.html">Blog left Sidebar</a></li>
							      		<li><a href="blog-v3.html">BLOG STANDARD</a></li>
							      		<li><a href="blog-v4.html">BLOG MASONRY</a></li>
							            <li><a href="blog-v5.html">Blog IMAGE COVER</a></li>
							            <li><a href="blog-v6.html">BLOG FULLWIDTH</a></li>
							            <li><a href="blog-v7.html">Video Blog</a></li>
							            <li><a href="#">Other layout</a>
							      			<ul>
							      				<li><a href="archives.html">Date archives</a></li>
							      				<li><a href="catergories.html">catergories archives</a></li>
							      			</ul>
							      		</li>
							         </ul>
							      </li> -->
							      <!-- <li><a href="#">Single post</a>
							      	<ul class="dropdown">
							            <li><a href="blog-details-v1.html">Standard Single post</a></li>
							            <li><a href="blog-details-v2.html">Gallery single post</a></li>
							            <li><a href="blog-details-v3.html">Gallery with lightbox</a></li>
							            <li><a href="blog-details-v4.html">Video post</a></li>
							            <li><a href="blog-details-v5.html">Fullwidth post</a></li>
							            <li><a href="blog-details-v6.html">Classic Single post</a></li>
							         </ul>
							      </li> -->

							   </ul>
							</nav> <!-- /#mega-menu-holder -->
				   		</div> <!-- /.menu-wrapper -->
					</div> <!-- /.content-holder -->
				</div> <!-- /.container -->
			</header>
<!-- End Main Header -->


<!--Mobile Menu-->
<?php /**PATH D:\xampp\htdocs\zionweb\resources\views/layouts/frontLayout/front_header.blade.php ENDPATH**/ ?>