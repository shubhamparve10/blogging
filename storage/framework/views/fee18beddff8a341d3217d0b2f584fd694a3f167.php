
    <style>
    .card {
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
        top:30px;
        width:40%;
}</style>
<?php $__env->startSection('content'); ?>

<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Blog Section</h1>
            <?php if(Session::has('flash_message_error')): ?>
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_error'); ?></strong>
          </div>
      <?php endif; ?>   
      <?php if(Session::has('flash_message_success')): ?>
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_success'); ?></strong>
          </div>
      <?php endif; ?>   
          </div> 
        </div> 
      </div> 
    </div>
    <!-- /.content-header -->

    <section class="content">
    <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>All Blogs</h5>
            <a href="<?php echo e(url('/admin/add-blog')); ?>"><button style="float: right; margin: 3px 3px;" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Blog</button></a>
          </div>
          <div class="widget-content nopadding table table-responsive">
            <table class="table table-bordered data-table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Image</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

                <?php $__currentLoopData = $blogAll; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="gradeX">
                  <td><?php echo e($loop->index+1); ?></td>
                  <td style="text-align: center;"><?php echo e(Str::limit($blog->name, 20)); ?> </td>
                   <td style="text-align: center;width: 50%;"><?php echo Str::limit(strip_tags($blog->content),20); ?></td>
                   


                  <td style="text-align: center;">
                    <?php if(!empty($blog->image)): ?>
                    <img src="<?php echo e(asset('/images/backend_images/blog/'.$blog->image)); ?>" style="height: 50px;">
                    <?php endif; ?>
                  </td>
                  <td style="text-align: center;"><?php echo e(date('d M Y', strtotime($blog->blog_date))); ?></td>

                  <td class="center" style="text-align: center !important;">
                    <a href="#myModal<?php echo e($blog->id); ?>" data-toggle="modal" class="btn btn-success btn-mini" title="View Blog">View</a>
                    <a href="<?php echo e(url('/admin/edit-blog/'.$blog->id)); ?>" class="btn btn-primary btn-mini" title="Edit Blog ">Edit</a>
                    <a onclick="return confirm('Are you sure you want to delete this Blog?');" href="<?php echo e(url('/admin/delete-blog/'.$blog->id)); ?>" class="btn btn-danger btn-mini" title="Delete product">Delete</a>
                 </td>
                </tr>
                <div id="myModal<?php echo e($blog->id); ?>" class="modal hide">
                  <!-- <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>"<?php echo e($blog->name); ?>" Full Details</h3>
                    <h3>"<?php echo $blog->blog_date; ?>"  </h3>
                    <h3>"<?php echo $blog->content; ?>"  </h3> -->
                    <div class="card" style="width: 70rem;  " id="myModal<?php echo e($blog->id); ?>">
  <div class="card-body">
  <button data-dismiss="modal" class="close" type="button">×</button>

  <b>Name : </b>    <h3><?php echo e($blog->name); ?>  </h3><br>
  <b>Date : </b>             <h3><?php echo $blog->blog_date; ?>  </h3><br>
  <b>Content : </b>          <h3><?php echo $blog->content; ?> </h3><br>
  <b>Image : </b>          <h3><center><img class="card"src="<?php echo e(asset('/images/backend_images/blog/'.$blog->image)); ?>"></center>  </h3><br>

  </div>
                     
                  </div>

                  
                  <!-- <div class="modal-body">
                    <p><b>Blog ID: </b><?php echo e($blog->id); ?></p>
                    <p><b>Name : </b><?php echo $blog->name ?></p>
                    <p><b>Date : </b><?php echo $blog->blog_date ?></p>
                    <p><b>Content : </b><?php echo $blog->content; ?></p>
                    <p><b>Created On: </b><?php echo e(date('D, d M Y, h:i a', strtotime($blog->created_at))); ?></p>
                    <p><b>Updated On: </b><?php echo e(date('D, d M Y, h:i a', strtotime($blog->updated_at))); ?></p>
                      <p><b>Image: </b></p>
                     <center><img src="<?php echo e(asset('/images/backend_images/blog/'.$blog->image)); ?>"></center>  
                  </div> -->
                </div>  
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
              </tbody>
            </table> 
    </section>

    </div>
    <br>


 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix\resources\views/admin/blog/view_blogs.blade.php ENDPATH**/ ?>