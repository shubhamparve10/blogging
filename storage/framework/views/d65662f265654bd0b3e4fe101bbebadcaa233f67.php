
<?php $__env->startSection('content'); ?>
			<!-- ===================================================
				About Me Wrapper
			==================================================== -->
			<div class="about-me-wrapper">
				<div class="back-to-home"><a href="<?php echo e('/'); ?>"><i class="flaticon-left-arrow"></i> Back to home</a></div>
				<div class="main-text-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-xl-7 col-lg-8 col-12">
								<h2>Zion's Touch</h2>
								<h3>Artist,Blogger</h3>
								<p>Hello, my name is Zoe. A qualified Artist, Tutor and Healthcare professional. A diverse
Specialist with experience in Broadcasting, Teaching, Nursing, Acting and Directing.
Currently engaged as a YouTube content creator, Podcaster, and Blogger. My dedication to
my family despite several challenges faced, led to the creation of Zion’s Touch. My vision
and passion centres on uplifting the family as a unit and individually. As my podcast titled
“Dreams before Dreams” aims to inspire young and mature audiences, and my YouTube
content titled “The forgotten hero” caters to single mothers. I continue to work on other
engaging contents.

<!-- Many academics and professionals have said to me “Being a parent is hard work but being a
single parent to three children, is extremely challenging. How do you cope?”
Zion’s Touch aims to encourage and inspire you through your personal struggles. Offering
advice through entertainment to keep you positive and uplifted.. -->
</p>
								<ul>
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="https://www.youtube.com/channel/UCA6qp76Pk_3gr92yqZUyCog"><i class="fab fa-youtube"></i></a></li>

									<li><a href="#"><i class="fab fa-twitter"></i></a></li>

									<!-- <li><a href="#"><i class="fab fa-twitter"></i></a></li> -->
									<!-- <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
									<li><a href="#"><i class="fab fa-pinterest"></i></a></li>
									<li><a href="#"><i class="fab fa-dribbble"></i></a></li>
									<li><a href="#"><i class="fab fa-vimeo-v"></i></a></li> -->
									<!-- <li><a href="#"><i class="fab fa-behance"></i></a></li>
									<li><a href="contact.html"><i class="fas fa-at"></i></a></li> -->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div> <!-- /.about-me-wrapper -->



			<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\zionstouch\resources\views/about.blade.php ENDPATH**/ ?>