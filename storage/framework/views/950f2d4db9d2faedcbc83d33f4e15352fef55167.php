<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dashbord</title>
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/fontawesome-free/css/font-awesome.min.css')); ?>">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/fontawesome-free/css/all.min.css')); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')); ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/icheck-bootstrap/icheck-bootstrap.min.css')); ?>">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/jqvmap/jqvmap.min.css')); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/dist/css/adminlte.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/dist/css/adminlte.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/dist/css/adminlte.min.css.map')); ?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/daterangepicker/daterangepicker.css')); ?>">
  <!-- summernote -->
  <!-- <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/summernote/summernote-bs4.min.css')); ?>">
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script> -->
    <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/summernote/summernote-bs4.min.css')); ?>">

    <style>
    .whole-page-overlay{
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  position: fixed;
  background: rgba(0,0,0,0.6);
  width: 100%;
  height: 100% !important;
  z-index: 1050;
  display: none;
}
.whole-page-overlay .center-loader{
  top: 50%;
  left: 52%;
  position: absolute;
  color: white;
}
    </style>
 
   <!-- Font Awesome -->
   <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(url('backendfiles/dist/css/adminlte.min.css')); ?>">

  <script src="<?php echo e(asset('js/app.js')); ?>"></script>

 
</head>
<body class="hold-transition sidebar-mini layout-fixed">
 
<?php echo $__env->make('layouts/backLayout/back_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts/backLayout/back_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


<?php echo $__env->yieldContent('content'); ?>
<div class="whole-page-overlay" id="whole_page_loader">
<img class="center-loader"  style="height:100px;" src="https://example.com/images/loader.gif"/>
</div>
<?php echo $__env->make('layouts/backLayout/back_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


</body>
<!-- jQuery -->
<script src="<?php echo e(url('backendfiles/plugins/jquery/jquery.min.js')); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo e(url('backendfiles/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>        
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo e(url('backendfiles/plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
<!-- ChartJS -->
<script src="<?php echo e(url('backendfiles/plugins/chart.js/Chart.min.js')); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo e(url('backendfiles/plugins/sparklines/sparkline.js')); ?>"></script>
<!-- JQVMap -->
<script src="<?php echo e(url('backendfiles/plugins/jqvmap/jquery.vmap.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/jqvmap/maps/jquery.vmap.usa.js')); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo e(url('backendfiles/plugins/jquery-knob/jquery.knob.min.js')); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo e(url('backendfiles/plugins/moment/moment.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/daterangepicker/daterangepicker.js')); ?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo e(url('backendfiles/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')); ?>"></script>
<!-- Summernote -->
<script src="<?php echo e(url('backendfiles/plugins/summernote/summernote-bs4.min.js')); ?>"></script>
<!-- overlayScrollbars -->
<script src="<?php echo e(url('backendfiles/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(url('backendfiles/dist/js/adminlte.js')); ?>"></script>
<!-- <script src="<?php echo e(url('backendfiles/dist/js/adminlte.js.map')); ?>"></script>
<script src="<?php echo e(url('backendfiles/dist/js/adminlte.min.js')); ?>"></script> -->

<script src="<?php echo e(url('backendfiles/dist/js/adminlte.min.js.map')); ?>"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(url('backendfiles/dist/js/demo.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/matrix.tables.js')); ?>"></script>
 <script src="<?php echo e(url('backendfiles/dist/js/pages/dashboard.js')); ?>"></script>

<!-- 
<script type="text/javascript">
        $(document).ready(function() {
          $('.summernote').summernote();
        });
</script> -->

    <script src="<?php echo e(url('backendfiles/plugins/jquery/jquery.min.js')); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo e(url('backendfiles/plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>

<!-- DataTables  & Plugins -->
<script src="<?php echo e(url('backendfiles/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/datatables-responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/datatables-buttons/js/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/jszip/jszip.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/pdfmake/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/pdfmake/vfs_fonts.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/datatables-buttons/js/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/datatables-buttons/js/buttons.print.min.js')); ?>"></script>
<script src="<?php echo e(url('backendfiles/plugins/datatables-buttons/js/buttons.colVis.min.js')); ?>"></script>

<!-- AdminLTE App -->
<script src="<?php echo e(url('backendfiles/dist/js/adminlte.min.js')); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(url('backendfiles/dist/js/demo.js')); ?>"></script>
<!-- Page specific script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["csv", "excel", "pdf", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

<script src="<?php echo e(url('backendfiles/plugins/summernote/summernote-bs4.min.js')); ?>"></script>
    <script>
  $(function () {
    // Summernote
    $('#summernote').summernote()

    // CodeMirror
    CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
      mode: "htmlmixed",
      theme: "monokai"
    });
  })
</script>

<script src="<?php echo e(url('backendfiles/plugins/jquery-validation/jquery.validate.min.js')); ?>"></script>

<script>

</script>

</body>
</html>

</html>
<!-- "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"] --><?php /**PATH C:\xampp\htdocs\zionstouch\resources\views/layouts/backLayout/back_design.blade.php ENDPATH**/ ?>