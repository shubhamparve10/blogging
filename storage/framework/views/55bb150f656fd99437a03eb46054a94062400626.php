<?php $__env->startSection('content'); ?>
<style>
.news-block .lower-box h5 {
    font-size: 22px;
    margin-top:10px;
}
 
    .image-box {
    position: relative;
    display: block;
    overflow: hidden;
 
}
</style>
<!-- Banner Section -->
        <section class="page-banner">
            <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
            <div class="shape-1"></div>
            <div class="shape-2"></div>
            <div class="banner-inner">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <h1>Blog</h1>
                        <div class="page-nav">
                            <ul class="bread-crumb clearfix">
                                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                <li class="active">Blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Banner Section -->

        
<section class="services-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Title Block-->
            <!--<div class="title-block col-xl-12 col-lg-12 col-md-12 col-sm-12">-->
            <!--    <div class="inner">-->
            <!--        <div class="sec-title">-->
            <!--            <h2>We Shape the Perfect Solutions</h2>-->
            <!--            <div class="lower-text">We are committed to providing our customers with exceptional-->
            <!--                service while offering our employees the best training.</div>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->
            <!--Service Block-->
            <div class="row clearfix">
            <?php $__currentLoopData = $blogs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $blog): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            


           
                <!--News Blog1-->
                <div class="news-block col-lg-3 col-md-6 col-sm-12 wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                    <div class="inner-box">
                        <div class="image-box">
                            <a href="<?php echo e("blog".$blog->id); ?>">
                                <img src="<?php echo e(asset('/images/backend_images/blog/'.$blog->image)); ?>" alt="" style="height:280px;object-fit:cover">
                                <!-- <iframe width="370" height="254" src="https://www.youtube.com/embed/3CQOnK9RjVE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                            </a>
                        </div>
                        <div class="lower-box">
                        
                            <h5><a href="http://matrixstatic.ycstechsoft.com/blog1"><?php echo e($blog->name); ?> </a></h5>
                            </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
          

            <!--Service Block-->
            
            
            
            
            
        </div>
        
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/blog.blade.php ENDPATH**/ ?>