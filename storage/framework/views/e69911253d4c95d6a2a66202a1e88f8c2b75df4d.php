
<?php $__env->startSection('content'); ?>

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Banners</a> <a href="#" class="current">Add Banner</a> </div>
    <h1>Banners Section</h1>
    <?php if(Session::has('flash_message_error')): ?>
        <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong><?php echo session('flash_message_error'); ?></strong>
        </div>
    <?php endif; ?>   
    <?php if(Session::has('flash_message_success')): ?>
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong><?php echo session('flash_message_success'); ?></strong>
        </div>
    <?php endif; ?>   
  </div>
  <section class="content">
    <div class="row">
      <div class="col-md-9">
      <form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo e(url('/admin/add_banner')); ?>" name="add_blog" id="add_blog" novalidate="novalidate">
          <?php echo e(csrf_field()); ?>

        <div class="card card-primary">
            <div class="card-body">
            <div class="form-group">
              <label for="inputName"> Tadd img</label>
              <input type="file" name="image" id="image">
            </div>

            <div class="form-group">
            <label for="inputName"> Image </label>
      <div class="controls">
        <input type="file" name="image" id="image"  class="form-control">
      </div>
    </div>



            <div class="form-group">
              <label for="inputDescription">Content</label>
              <textarea type="text" name="heading" id="heading" style="width: 65%">Heading</textarea>             </div>
            
            <div class="form-group">
              <label for="inputDate">link</label>
              <input type="text" name="link" id="link" style="width: 65%">
            </div>
            <div class="form-group">
              <label for="inputDate">title</label>
              <input type="text" name="link" id="link" style="width: 65%">

              <div class="form-group">
                <label for="inputDate">enable</label>
                <input type="checkbox" name="status" id="status" class="btn btn-success" value="1">
              </div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      
    </div>
    <div class="row">
      <div class="col-9">
        <a href="#" class="btn btn-secondary">Cancel</a>
        <input type="submit"  value="Add Blog" class="btn btn-success float-right">
</form>
      </div>
    </div>  

<?php $__env->stopSection(); ?>


 
<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix\resources\views/admin/banners/add_banner.blade.php ENDPATH**/ ?>