
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>
    .view {
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 50px; /* Added */
        top:60px;
        width:40%;
}</style>

<?php $__env->startSection('content'); ?>

<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Blog Section</h1>
            <?php if(Session::has('flash_message_error')): ?>
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_error'); ?></strong>
          </div>
      <?php endif; ?>   
      <?php if(Session::has('flash_message_success')): ?>
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_success'); ?></strong>
          </div>
      <?php endif; ?>   
          </div> 
        </div> 
      </div> 
    </div>
    <!-- /.content-header -->

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"> All videos</h3>
                <a href="<?php echo e(url('/admin/add_video')); ?>"><button style="float: right; margin: 3px 3px;" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Video</button></a>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>#</th>                   
                  <th>Title</th>
                  <th>Link</th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="gradeX">
                <td style="text-align: center;"><?php echo e($loop->index+1); ?> </td>
                   <td style="text-align: center;"><?php echo e($video->title); ?></td>
                  <td style="text-align: center;"><a href="<?php echo e($video->link); ?>" target="_blank"><?php echo e($video->link); ?></a></td>
                

                  <td class="center" style="text-align: center !important;">
                    <a href="#myModal<?php echo e($video->id); ?>" data-toggle="modal" class="btn btn-success btn-mini" title="View Product">View</a>
                    <a href="<?php echo e(url('/admin/edit_video/'.$video->id)); ?>" class="btn btn-primary btn-mini" title="Edit Product ">Edit</a>
                   
                    <a id="delVideo" href="<?php echo e(url('/admin/delete_video/'.$video->id)); ?>" class="btn btn-danger btn-mini delete-confirm" title="Delete product">Delete</a>
                    
                 </td>
                </tr>
                <div id="myModal<?php echo e($video->id); ?>" class="modal hide">
                
                    <div class="view" style="width: 70rem;" id="myModal<?php echo e($video->id); ?>">
                      <div class="card-body " style="background: beige;">
                      <button data-dismiss="modal" class="close" type="button">×</button>
                      <b>Name : </b>    <h3><?php echo e($video->id); ?>  </h3><br>
                      <b>Title : </b>  <h3><?php echo e($video->title); ?> </h3><br>
                      <iframe width="100%" height="250px" src="https://www.youtube.com/embed/<?php echo e($video->link); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 
                      </div>
                     
                  </div>
 
                </div>  
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
 
</div>
<script>
    $('.delete-confirm').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Are you sure you want to delete this Video?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
</script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/admin/video/view_video.blade.php ENDPATH**/ ?>