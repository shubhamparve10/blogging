<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Matix Digital Technologies</title>
    <!-- Stylesheets -->
    <link href="https://fonts.googleapis.com/css2?family=Teko:wght@300;400;500;600;700&amp;display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/bootstrap.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/fontawesome-all.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/owl.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/flaticon.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/jquery-ui.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/jquery.fancybox.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/hover.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/custom-animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/frontend_css/style.css')); ?>" rel="stylesheet">
    <!-- rtl css -->
    <link href="<?php echo e(asset('css/frontend_css/rtl.css')); ?>" rel="stylesheet">
    <!-- Responsive File -->
    <link href="<?php echo e(asset('css/frontend_css/responsive.css')); ?>" rel="stylesheet">

    <!-- Color css -->
    <link rel="stylesheet" id="jssDefault" href="<?php echo e(asset('css/frontend_css/colors/color-default.css')); ?>">

    <link rel="shortcut icon" href="<?php echo e(asset('images/frontend_images/favicon.png')); ?>" id="fav-shortcut" type="image/x-icon">
    <link rel="icon" href="<?php echo e(asset('images/frontend_images/favicon.png')); ?>" id="fav-icon" type="image/x-icon">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Teko:wght@700&display=swap" rel="stylesheet">

    <!-- Responsive Settings -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <style>
        @media  screen and (max-width: 600px) {
              .zm {
                height: 300px !important;
              }
            }
            .teko{
                font-family: 'Teko', sans-serif !important;
                font-weight: 600;
            }
            .main-menu .navigation li > a{
                font-family: 'Teko', sans-serif !important;
                font-weight: 500;
            }
    </style>
    <style>
        @media  screen and (max-width: 600px) {
              .zm {
                height: 200px !important;
              }
            }
    </style>
</head>

<body>
    <div class="page-wrapper">
        <!-- Preloader -->
        <!-- <div class="preloader">
            <div class="icon"></div>
        </div> -->

        <?php echo $__env->make('layouts.frontLayout.front_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo $__env->yieldContent('content'); ?>

        <?php echo $__env->make('layouts.frontLayout.front_footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    </div>
    <!--End pagewrapper-->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>

    <script src="<?php echo e(asset('js/frontend_js/jquery.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/TweenMax.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/jquery-ui.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/jquery.fancybox.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/owl.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/mixitup.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/appear.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/wow.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/jQuery.style.switcher.min.js')); ?>"></script>
    <script type="text/javascript" src="../../cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.2/js.cookie.min.js">
    </script>
    <script src="<?php echo e(asset('js/frontend_js/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/frontend_js/custom-script.js')); ?>"></script>

    <script src="<?php echo e(asset('js/frontend_js/lang.js')); ?>"></script>
    <script src="../../translate.google.com/translate_a/elementa0d8.html?cb=googleTranslateElementInit"></script>
    <script src="<?php echo e(asset('js/frontend_js/color-switcher.js')); ?>"></script>
</body>
</html><?php /**PATH C:\xampp\htdocs\matrix\resources\views/layouts/frontLayout/front_design.blade.php ENDPATH**/ ?>