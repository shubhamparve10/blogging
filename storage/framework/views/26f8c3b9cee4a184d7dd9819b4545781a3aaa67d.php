<?php $__env->startSection('content'); ?>

<!-- Banner Section -->
        <section class="page-banner">
            <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
            <div class="shape-1"></div>
            <div class="shape-2"></div>
            <div class="banner-inner">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <h1>Videos</h1>
                        <div class="page-nav">
                            <ul class="bread-crumb clearfix">
                                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                <li class="active">Videos</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Banner Section -->

        <!-- News Section -->
        <section class="news-section">
            <div class="auto-container">

                <div class="row clearfix">
                <?php $__currentLoopData = $allvideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0ms"
                        data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="image-box">
                                <a href="<?php echo e($video->link); ?>"> 
                                <iframe width="100%" height="250px" src="https://www.youtube.com/embed/<?php echo e($video->link); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </a>
                            </div>
                            <div class="lower-box">
                                <div class="post-meta">
                                    <ul class="clearfix">
                                        
                                    </ul>
                                </div>
                                <h5><a href="<?php echo e($video->link); ?>"><?php echo e($video->title); ?></a></h5>
                              
                            </div>
                        </div>
                    </div>
                   
                     
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <!-- <div class="more-box">
                    <a class="theme-btn btn-style-one" href="blog.html.html">
                        <i class="btn-curve"></i>
                        <span class="btn-title">Load more posts</span>
                    </a>
                </div> -->
            </div>
        </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/videos.blade.php ENDPATH**/ ?>