
    <style>
    .view {
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
        top:30px;
        width:40%;
}</style>
<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Placement Section</h1>
            <?php if(Session::has('flash_message_error')): ?>
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_error'); ?></strong>
          </div>
      <?php endif; ?>   
      <?php if(Session::has('flash_message_success')): ?>
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_success'); ?></strong>
          </div>
      <?php endif; ?>   
          </div> 
        </div> 
      </div> 
    </div>   

<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"> </h3>
                <a href="<?php echo e(url('/admin/add-placement')); ?>"><button style="float: right; margin: 3px 3px;" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Placement</button></a>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Company Name</th>
                  <th>Role</th>
                   <th>Image</th>
                  <th>Date</th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  Placement
                <?php $__currentLoopData = $placementAll; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $placement): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="gradeX">
                  <td><?php echo e($loop->index+1); ?></td>
                  <td style="text-align: center;width: 15%;"><?php echo e(Str::limit($placement->name, 20)); ?> </td>
                   <td style="text-align: center;width: 20%;"><?php echo Str::limit(strip_tags($placement->cname),20); ?></td>
                   <td style="text-align: center;width: 20%;"><?php echo Str::limit(strip_tags($placement->role),20); ?></td>


                  <td style="text-align: center;">
                    <?php if(!empty($placement->image)): ?>
                    <img src="<?php echo e(asset('/images/backend_images/placement/'.$placement->image)); ?>" style="height: 50px;">
                    <?php endif; ?>
                  </td>
                  <td style="text-align: center;"><?php echo e(date('d M Y', strtotime($placement->placement_date))); ?></td>

                  <td class="center " style="text-align: center !important; width: 25%;">
                    <a href="#myModal<?php echo e($placement->id); ?>" data-toggle="modal" class="btn btn-success btn-mini" title="View Placement">View</a>
                    <a href="<?php echo e(url('/admin/edit-placement/'.$placement->id)); ?>" class="btn btn-primary btn-mini" title="Edit Placement ">Edit</a>
                    <a onclick="return confirm('Are you sure you want to delete this placement?');" href="<?php echo e(url('/admin/delete-placement/'.$placement->id)); ?>" class="btn btn-danger btn-mini" title="Delete product">Delete</a>
                 </td>
                </tr>
                <div id="myModal<?php echo e($placement->id); ?>" class="modal hide">
                
                    <div class="view" style="width: 70rem;" id="myModal<?php echo e($placement->id); ?>">
                      <div class="card-body " style="background: beige;">
                      <button data-dismiss="modal" class="close" type="button">×</button>

                      <b>Name : </b>    <h3><?php echo e($placement->name); ?>  </h3><br>
                      <b>Company Name : </b><h3><?php echo e($placement->cname); ?>  </h3><br>
                      <b>Role: </b>    <h3><?php echo e($placement->role); ?>  </h3><br>                
                      <b>Date : </b>  <h3><?php echo $placement->placement_date; ?>  </h3><br>
                      <b>Image : </b>   <h3><center><img class="view"src="<?php echo e(asset('/images/backend_images/placement/'.$placement->image)); ?>"></center>  </h3><br>

                      </div>
                     
                  </div>
 
                </div>  
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>

    </div>
<?php $__env->stopSection(); ?>
 


 
<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/admin/placement/view_placement.blade.php ENDPATH**/ ?>