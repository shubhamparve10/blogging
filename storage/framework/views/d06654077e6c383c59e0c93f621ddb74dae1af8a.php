<?php $__env->startSection('content'); ?>

<!-- Banner Section -->
        <section class="page-banner">
            <div class="image-layer" style="background-image:url(images/frontend_images/main-slider/2.jpg);"></div>
            <div class="shape-1"></div>
            <div class="shape-2"></div>
            <div class="banner-inner">
                <div class="auto-container">
                    <div class="inner-container clearfix">
                        <h1>Fundamental course on advanced data science with python</h1>
                        <div class="page-nav">
                            <ul class="bread-crumb clearfix">
                                <li><a href="<?php echo e(url('/')); ?>">Home</a></li>
                                <li class="active">Fundamental course on advanced data science with python</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Banner Section -->



</section>
<!-- Team Section -->
        <section class="team-section">
            <div class="auto-container">
                        <div class="sec-title  ">
                                    <h2>Free Course on Fundamentals Of Data Science </h2>
                                       <p class="mon">Before settling down for a course, get a teaching experience from our trainers.
                                Enrol a seat for a free demo session of Full-stack digital marketing program now.  </p>
                        </div>
                       
                        <div class="sec-title centered">
                                    <h2>About our trainers</h2>
                    
                        </div>
                <div class="row clearfix">
                    <!--Team-->
                    <div class="team-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="image-box">
                            <img src="<?php echo e(asset('images/frontend_images/resource/team-1.jpg')); ?>" alt="">
                               
                            </div>
                            <div class="lower-box" style="padding: 15px;">
                                <h5>Govind Chandak</h5>
                                <div class="designation"><h7>Founder – Matrix Digital Technologies</h7></div>
                            </div>
                        </div>
                        <div class="text-content">
                           <p class="mon">Govind Chandak believes in inspiring and motivating while educating business, tools and technologies of digital marketing to make the trained students stand out in the crowd and reach millennial and generation consumers.
                            </p>   
                        </div>
                    </div>

                    <!--Team-->
                    <div class="team-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="image-box">
                                <img src="<?php echo e(asset('images/frontend_images/resource/team-2.jpg')); ?>" alt="">
                                
                            </div>
                            <div class="lower-box" style="padding: 15px;">
                                <h5>Mr. Ramkumar Driya</h5>
                                <div class="designation"><h7>Data Science Mentor - NSDM INDIA</h7></div>
                            </div>
                        </div>
                        <div class="text-content">
                         <p class="mon">Omkar is a Founder of NSEMBLE.AI,, who guides students with entrepreneurial opportunities in AI Domain. Neural network and machine learning.</p>

                        </div>
                    </div>

                    <!--Team-->
                    <div class="team-block col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="image-box">
                                <img src="<?php echo e(asset('images/frontend_images/resource/team-3.jpg')); ?>" alt="">
                                
                            </div>
                            <div class="lower-box" style="padding: 15px;">
                                <h5>MR. OMKAR MOHASHI</h5>
                                <div class="designation"><h7>Data Science Mentor - NSDM INDIA</h7></div>
                            </div>
                        </div>
                        <div class="text-content">
                                      <p class="mon">Ramkumar Dhanoriyais a neural network specialist carry research experience in machine learning and deep learning. He helps students to understand and analyse businesses using cutting edge, AI, ML and deep learning technologies. 
                                      </p>
                        
                                  </div>
                    </div>

               
                </div>
            </div>
            <!----Team end--->
            <div class="sidebar-page-container">
            <div class="auto-container">
                
                
                <div class="row clearfix">

                    


                    <!--Sidebar Side-->
                    <!-- <div class=" col-lg-3 col-sm-12"></div> -->
                    <div class="sidebar-side col-lg-12 col-sm-12 ">
                        <aside class="sidebar blog-sidebar">

                         <div class="sec-title centered">
                                        <h3>BOOK MY SLOT</h3>
                          </div>                           

                  <div class="sidebar-widget call-up">
                         <div class="widget-inner">
                                    <div class="sidebar-title centered">
                                        <h4>Webinar Register form for Digital marketing Demo-Nagpur branch</h4>
                                    </div>
                             <div class="form-box">
                     
                                 <div class="default-form">
                                    <form method="post" action="http://layerdrops.com/linoorhtml/sendemail.php" id="contact-form">
                                        <div class="row clearfix">
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="text" name="name" value="" placeholder="Name" required="">
                                                </div>
                                            </div>
                                            <!-- <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="text" name="lastname" value="" placeholder="Last Name" required="">
                                                </div>
                                            </div> -->
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="text" name="phone" value="" placeholder="Phone Number" required="">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="email" name="email" value="" placeholder="Email Address"
                                                        required="">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12">
                                                <div class="field-inner">
                                                    <input type="text" name="city" value="" placeholder="City Name"
                                                        required="">
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group col-lg-12 col-md-12 col-sm-12 centered">
                                                <button class="theme-btn btn-style-one ">
                                                    <i class="btn-curve"></i>
                                                    <span class="btn-title">submit</span>
                                                </button>
                                            </div>
                                        </div>
                                      </form>
                                  </div>
                            </div>  
                                </div>
            </div>
            </div>
                        </aside>
                    </div>
              

                  
                    <div class="content-side col-lg-12 col-md-12 col-sm-12">
                        <div class="service-details">
                            <div class="text-content">
                                <h3>What makes us stand out?</h3>
                                  <p class="mon">
                                    <i class="fa fa-check" aria-hidden="true"></i>   Digital marketing classes are conducted with a maximum of 6 students in a batch
                                        ensuring personal attention to every candidate.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   100% placement assurance.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   13+ Certificates.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Mock interviews are conducted to crack the interviews in the toughest 8companies.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Lifetime LMS access – Our recorded sessions of the digital marketing course are   
                                        present in LMS.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Weekly 1:1 mentoring session to understand the individual goals of every student to 
                                        provide them with the righteous path.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Trainers are from the high position of the digital marketing industry.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Guest lectures from the thought leaders.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   24*7 Facebook Community Support.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Career transition program by standard school Alumni Faculty.
                                    <i class="fa fa-check" aria-hidden="true"></i>   Global standard curriculum.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Access to all the webinars and boot camps.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Access to 50+ tools.<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Hands on practise on 27+ modules, live projects, 25 assignments and 6 quizzes.

                                    </p>
                            </div><br>
                        </div>
                    </div>
                    <div class="content-side col-lg-12 col-md-12 col-sm-12">
                        <div class="service-details">
                            <div class="text-content">
                                <h3>Is this for me?</h3>
                                    <p  class="mon">
                                        Our digital marketing course is conducted from the beginning level, hence anyone who is passionate about online market can take the digital marketing training.
                                    </p>
                                    <p  class="mon">
                                    <i class="fa fa-check" aria-hidden="true"></i>   Graduates<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Post Graduates<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Students<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Management Students<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Working Professionals<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Start Up Founders<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>   Entrepreneurs<br>
                                    
                                    </p>
                            </div>
                        </div>
                    </div> 
                     <div class="content-side col-lg-12 col-md-12 col-sm-12">
                        <div class="service-details">
                            <div class="text-content">
                                <h3>What do the participants say?</h3>
                                    <p  class="mon">Our data science training is conducted from a primary level and in a complete non-coder way to ensure a person with basic technical background can also understand the concepts.</p>
                                    <p  class="mon">
                                    <i class="fa fa-check" aria-hidden="true"></i>Engineering Students<br>
                                    <i class="fa fa-check" aria-hidden="true"></i> IT Professionals<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>Software Developers<br>
                                    <i class="fa fa-check" aria-hidden="true"></i>Management Students<br>
                                   <i class="fa fa-check" aria-hidden="true"></i> Digital Marketing Professionals<br>
                                    <i class="fa fa-check" aria-hidden="true"></i> Professionals with logical, mathematical and analytical skills<br>
                                    <i class="fa fa-check" aria-hidden="true"></i> Business Intelligence professionals<br>
                                    <i class="fa fa-check" aria-hidden="true"></i> Business Analysts<br>
                                    <i class="fa fa-check" aria-hidden="true"></i> Product Managers
        
                                    </p>
                            </div>
                        </div>
                    </div> 
                    

                </div>
            </div>
        </div>
           
              <div class="auto-container">
        <div class="sec-title">
            <h2>What do the participants say?</h2>
        </div>
        <div class="carousel-box">
            <div class="testimonials-carousel owl-theme owl-carousel">
                <div class="testi-block">
                    <div class="inner">
                        <div class="icon"><span>“</span></div>
                        <div class="info">
                            <!--<div class="image"><a href="#"><img src="<?php echo e(asset('images/frontend_images/resource/author-1.jpg')); ?>"alt=""></a></div>-->
                            <div class="name">Raveena Chhabrani </div>
                            <div class="designation">Media Intern</div>
                        </div>
                        <div class="text">
                            <p class="mon">
                            What an amazing journey we had at NSDM-India. I can proclaim it without a doubt and with lots of research that they are best in Market, when it comes to the training of Digital Marketing. Just go & Govind Sir will take care of the rest, of course u need to do hard and smart work. But there is always need of strong support pillar, & he is the one for his students. At NSDM-India u learn more than just Digital Marketing. They also very supportive for placements as well. Take my word, YOUR PROSPECTIVE OVER THINGS WOULD CHANGE ONCE U COMPLETE THE COURSE.
                           </p>
                        </div>
                    </div>
                </div>
                <div class="testi-block">
                    <div class="inner">
                        <div class="icon"><span>“</span></div>
                        <div class="info">
                            <!--<div class="image"><a href="#"><img src="<?php echo e(asset('images/frontend_images/resource/author-2.jpg')); ?>"-->
                            <!--            alt=""></a></div>-->
                            <div class="name">Merkel Sokrati</div>
                            <!-- <div class="designation">Director</div> -->
                        </div>
                        <div class="text">
                            <p class="mon">
                            NSDM Nagpur Is One Of The Best Digital Marketing Institute in Nagpur. If You Are Looking For Digital Marketing Training Institute In Nagpur NSDM is Best.They assured 100% Placement.Thanks To Govind Sir For their Co-ordination.
                          </p>
                    </div>
                   </div>
               </div>
                <div class="testi-block">
                    <div class="inner">
                        <div class="icon"><span>“</span></div>
                        <div class="info">
                            <!--<div class="image"><a href="#"><img src="<?php echo e(asset('images/frontend_images/resource/author-3.jpg')); ?>"-->
                            <!--            alt=""></a></div>-->
                            <div class="name">Girisha Sanghavi </div>
                            <div class="designation">Infrrd Private.Ltd </div>
                            <div class="designation">Data & Business Analyst</div>
                        </div>
                        <div class="text">
                            <p class="mon">As per I have experienced
                                My experience says that this is one of the best digital marketing institute. I learnt many things at a proffestional platform . At last I would rather say that NSDM is best education platform to study from.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="testi-block">
                    <div class="inner">
                        <div class="icon"><span>“</span></div>
                        <div class="info">
                            <!--<div class="image"><a href="#"><img src="<?php echo e(asset('images/frontend_images/resource/author-1.jpg')); ?>"-->
                            <!--            alt=""></a></div>-->
                            <div class="name">Ayushi LIlhare</div>
                            <div class="designation">HCL Technologies</div>
                            <div class="designation">Data Analyst & Client Management </div>
                        </div>
                        <div class="text">
                            <p class="mon">Had amazing experience in learning and exploring digital marketing and explains every concept with live examples that are easy to understand. Thank you Govind sir.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="testi-block">
                    <div class="inner">
                        <div class="icon"><span>“</span></div>
                        <div class="info">
                            <!--<div class="image"><a href="#"><img src="<?php echo e(asset('images/frontend_images/resource/author-2.jpg')); ?>"-->
                            <!--            alt=""></a></div>-->
                            <div class="name">Anagha Choudhari</div>
                            <div class="designation">HCL Technologies</div>
                            <div class="designation">Data Analyst </div>
                        </div>
                        <div class="text">
                            <p class="mon">There are many variations of passages of lorem ipsum available but the
                            majority have suffered alteration in some form, by injected humour, or randomised
                            words which don't look even slightly believable.
                          </p>
                    </div>
                    </div>
                </div>
                <div class="testi-block">
                    <div class="inner">
                        <div class="icon"><span>“</span></div>
                        <div class="info">
                            <!--<div class="image"><a href="#"><img src="<?php echo e(asset('images/frontend_images/resource/author-3.jpg')); ?>"-->
                            <!--            alt=""></a></div>-->
                            <div class="name">Prabhdeep Singh Sethi</div>
                            <div class="designation">Location Guru</div>
                            <div class="designation">Digital Marketing Intern</div>
                        </div>
                        <div class="text">
                            <p class="mon">They offer one of the best digital marketing course with overall development of the students.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
        <!-- Call To Section -->
        <section class="call-to-section-two alternate mb-4">
            <div class="auto-container">
                <div class="inner clearfix">
                    <h2>For more details contact/WhatsApp us</h2>
                    <div class="link-box">
                        <a class="theme-btn btn-style-two" "href="tel:9284960102"">
                            <i class="btn-curve"></i>
                            <span class="btn-title">9284960102</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
       



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/webinar2.blade.php ENDPATH**/ ?>