

<!-- 
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script> -->
    <!-- summernote css/js -->
<!-- <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<script type="text/javascript">
    $('#summernote').summernote({
        height: 200
    });
</script> -->

<?php $__env->startSection('content'); ?>

<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Blog Section</h1>
            <?php if(Session::has('flash_message_error')): ?>
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_error'); ?></strong>
          </div>
      <?php endif; ?>   
      <?php if(Session::has('flash_message_success')): ?>
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong><?php echo session('flash_message_success'); ?></strong>
          </div>
      <?php endif; ?>   
          </div> 
        </div> 
      </div> 
    </div>
    <!-- /.content-header -->

    <section class="content">
              <div class="row">
                <div class="col-md-9">
                <form enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo e(url('/admin/add-blog')); ?>" name="add_blog"  >
                    <?php echo e(csrf_field()); ?>

                  <div class="card card-primary">
                      <div class="card-body">
                      <div class="form-group">
                        <label for="inputName"> Title Name</label>
                        <input type="text"   name="name" id="name"   class="form-control" required>
                      </div>
 
                      <div class="form-group">
                      <label for="inputName"> Image </label>
                <div class="controls">
                  <input type="file" name="image" id="image"  class="form-control"required>
                </div>
              </div>



                      <div class="form-group">
                        <label for="inputDescription">Content</label>
                        <!-- <textarea name="content" id="inputDescription" class="form-control" rows="4" required></textarea> -->
                        <!-- <textarea class="summernote form-control"  id="summernote"name="content" required></textarea> -->
                        <textarea id="summernote"name="content" required></textarea>

                      </div>
                      
                      <div class="form-group">
                        <label for="inputDate">Date</label>
                        <input type="date" name="blog_date" id="blog_date" class="form-control"required>
                      </div>
                      
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                </div>
                
              </div>
              <div class="row">
                <div class="col-9">
                  <a href="<?php echo e(('/admin')); ?>" class="btn btn-secondary">Cancel</a>
                  <input type="submit"  value="Add Blog" class="btn btn-success float-right">
        </form>
                </div>
              </div>  
    </section>

    </div>
    <br>

 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/backLayout/back_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\matrix1\resources\views/admin/blog/add_blog.blade.php ENDPATH**/ ?>