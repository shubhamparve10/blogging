<?php $__env->startSection('content'); ?>
			<!-- ===================================================
				About Me Wrapper
            ==================================================== -->


			<div class="about-me-wrapper desktop">
                <div class="back-to-home1"><a href="<?php echo e(url('/')); ?>"><i class="flaticon-left-arrow"></i> Back to home</a></div>

				<div class="main-text-wrapper">
					<div class="container-fluid">
						<div class="row  ml-4">
							<div class="col-xl-7 col-lg-8 col-12 mt-4">
								<h2>Zion's Touch</h2>
								<h3>Artist,Blogger</h3>
								<p style="text-align:justify;">Hello, my name is Zoe. A qualified Artist, Tutor and Healthcare professional. A diverse
                                Specialist with experience in Broadcasting, Teaching, Nursing, Acting and Directing.
                                Currently engaged as a YouTube content creator, Podcaster, and Blogger. My dedication to
                                my family despite several challenges faced, led to the creation of Zion’s Touch. My vision
                                and passion centres on uplifting the family as a unit and individually. As my podcast titled
                                “Dreams before Dreams” aims to inspire young and mature audiences, and my YouTube
                                content titled “The forgotten hero” caters to single mothers. I continue to work on other
                                engaging contents.<br>
                                Many academics and professionals have said to me “Being a parent is hard work but being a
                                single parent to three children, is extremely challenging. How do you cope ?”
                                Zion’s Touch aims to encourage and inspire you through your personal struggles. Offering
                                advice through entertainment to keep you positive and uplifted.

                                </p>
							</div>
						</div>
					</div>
				</div>
            </div>

<div class="mb">
<p style="  top:20px;
left:15px;
z-index: 9;"><a href="<?php echo e(url('/')); ?>"><i class="flaticon-left-arrow"></i> Back to home</a>
</h1>
<img src="<?php echo e(url('images/home/mb1.jpg')); ?>" style="height: 500px; width:100%;" >
<p style="text-align:justify; padding:20px; color:#000;">Hello, my name is Zoe. A qualified Artist, Tutor and Healthcare professional. A diverse
    Specialist with experience in Broadcasting, Teaching, Nursing, Acting and Directing.
    Currently engaged as a YouTube content creator, Podcaster, and Blogger. My dedication to
    my family despite several challenges faced, led to the creation of Zion’s Touch. My vision
    and passion centres on uplifting the family as a unit and individually. As my podcast titled
    “Dreams before Dreams” aims to inspire young and mature audiences, and my YouTube
    content titled “The forgotten hero” caters to single mothers. I continue to work on other
    engaging contents.<br>
    Many academics and professionals have said to me “Being a parent is hard work but being a
    single parent to three children, is extremely challenging. How do you cope ?”
    Zion’s Touch aims to encourage and inspire you through your personal struggles. Offering
    advice through entertainment to keep you positive and uplifted.
</p>
</div>


<style>
    @media  only screen and (max-width:767px) {
        .desktop{
            display: none !important;
        }
    }


    @media  only screen and (min-width:767px) {
        .mb{
            display: none  !important;

        }
    }

    </style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontLayout.front_design', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\zionweb\resources\views/about.blade.php ENDPATH**/ ?>