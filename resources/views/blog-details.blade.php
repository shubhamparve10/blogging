@extends('layouts.frontLayout.front_design')
@section('content')
<style>
 .ex1 {
  
  height: 370px;
   overflow-y: scroll;
}

</style>
<!-- <div class="back-to-home1" ><a href="{{ url('/') }}" style="color:black"><i class="flaticon-left-arrow"></i> Back to Blog</a></div> -->

			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-12">

						<div class="blog-details">
							<div class="main-post-wrapper">
								<div class="post-top-image"><img class="view"src="{{ asset('/images/backend_images/blog/'.$blog->image) }}"></div>
								<h3 class="title">{{ $blog->name }} </h3>
								<ul class="author-meta clearfix">
									<!-- <li class="tag"><a href="#">Lifestyle</a></li> -->
                                    <li class="date"><a href="#">
									{{ date('d F Y | h:iA', strtotime($blog->blog_date)) }}
                                  </a></li>
									<li><button>  <div  class="addthis_inline_share_toolbox"></div></button></li>
								</ul>
								<p style="text-align:justify;"> {!! $blog->content !!} </p>

							</div> <!-- /.main-post-wrapper -->
						</div>
						<!-- /.blog-details -->
                        @if($comments->isEmpty())
                        <div class="details-page-inner-box comment-meta mb-0">
                        <h3>Be the first to comment this blog.</h3>
                        </div>
                        @else

                        <div class="details-page-inner-box comment-meta ex1 ">
							<h3>Comments</h3>
							<div class="single-comment clearfix">
							@foreach($comments as $comments)
								<!-- <img src="images/blog/50.jpg" alt="" class="float-left"> -->
								<div class="comment float-left">
									<h6><a href="#">{{ $comments->name }}</a></h6>
                                    <div class="date"> {{ date('d F Y | h:iA', strtotime($comments->created_at)) }}  </div>
										
									<!-- <a href="#" class=""></a> -->	
									<p>{{ $comments->comment }}</p>
								</div>
								@endforeach

							</div> <!-- /.single-comment -->

                        </div> <!-- /.comment-meta -->
                        @endif
						<div class="details-page-inner-box comment-form">
							<h3>Leave A Comment </h3>

							<form action="{{ url('/comments') }}" method="post" >@csrf
 								<div class="row">
						         <div class="col-12"> <input type="hidden"  name="blog_id" value="{{ $blog->id }}"></div>
									<div class="col-xl-6 col-lg-6 col-md-6 col-12"><input type="text" placeholder="YOUR NAME" name="name" required></div>
									<div class="col-xl-6 col-lg-6 col-md-6 col-12"><input type="email" placeholder="YOUR EMAIL" name="email" required></div>
									<!-- <div class="col-xl-4 col-lg-12 col-md-4 col-12"><input type="text" placeholder="WEBSITE"></div> -->
									<div class="col-12"><textarea placeholder="MESSAGE" name="comment" required></textarea></div>
							</div>
								<button class="theme-button-one">Post Comment</button>
							</form>
						</div> <!-- /.comment-form -->
					</div> <!-- /.col- -->




					<!-- ======================== Theme Sidebar =============================== -->
					<div class="col-lg-4 col-md-7 col-12 theme-main-sidebar">
						<div class="sidebar-box bg-box about-me">
							<h6 class="sidebar-title">About me</h6>
							<img src="{{ asset('images/home/1.jpg')}}" alt="">
							<p>Hello, my name is Zoe. A qualified Artist, Tutor and Healthcare professional.
								A diverse Specialist with experience in Broadcasting, Teaching, Nursing, Acting and Directing.</p>
							<!-- <div class="clearfix"><img src="images/home/sign.png" alt="" class="signature float-right"></div> -->
						</div> <!-- /.about-me -->

						<div class="sidebar-box bg-box sidebar-trending-post">
							<h6 class="sidebar-title">Popular Broadcasts</h6>
							<div class="single-trending-post clearfix">
								<img src="{{ url('images/home/th1.webp') }}" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="https://youtu.be/fBcUbxm7vcI" target="_blank">Love at Christmas (Part One) l Zion's Touch</a></h6>

								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->
							<div class="single-trending-post clearfix">
								<img src="{{ url('images/home/th4.webp') }}" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="https://youtu.be/1IiLwg58EmM"  target="_blank">Communicate! Love, Romance and Time l Zion's Touch</a></h6>

								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->
							<div class="single-trending-post clearfix">
								<img src="{{ url('images/home/th5.webp') }}" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="https://youtu.be/fmR0UR_rM_8"  target="_blank">Copy of Finally Monetized! Challenges I Overcame l Zion's Touch</a></h6>

								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->

						</div>


						 <!-- /.sidebar-newsletter -->
					</div> <!-- /.theme-main-sidebar -->
				</div> <!-- /.row -->
			</div> <!-- /.container -->

@endsection

