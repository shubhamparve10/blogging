<?php
    use App\Blog;
    use App\Video;
 

    $blog = Blog::count();
    $video = Video::count();
 
?>

 
   <!-- Main Sidebar Container -->
   <aside class="main-sidebar sidebar-dark-primary elevation-4 ">
    <!-- Brand Logo -->
    <!-- <a href="{{(('/'))}}" class="brand-link">
      <img src="{{ asset('images/logo/logo.png') }}" alt=" Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">  </span>
    </a>
     -->

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src=" " class="img-circle elevation-2 " alt="User Image">
          <i class="fas fa-user mr-2"></i>
        </div>
        <div class="info">
          <a href="#" class="d-block">   {{ Auth::user()->name }}</a>
        </div>
      </div> -->
<br>
      <!-- SidebarSearch Form -->
      <!-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item  ">
            <a href="{{('/admin')}}" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
           
          </li>
           
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa fa-comment"></i>
              <p> Blog
                 <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right"><?php echo $blog ?> </span>
 
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{('/admin/add-blog')}}" class="nav-link">
                  <i class="fa fa-arrow-right nav-icon"></i>
                  <p>Add Blog</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{('/admin/view-blogs')}}" class="nav-link">
                  <i class="fa fa-arrow-right nav-icon"></i>
                  <p>View Blog</p>
                </a>
              </li>
               
            </ul>
          </li>
          <!-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa fa-video"></i>
              <p>
               Video
               <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right"><?php echo $video ?> </span>
 
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{('/admin/add_video')}}" class="nav-link">
                  <i class="fa fa-arrow-right nav-icon"></i>
                  <p>Add Video</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{('/admin/view_video')}}" class="nav-link">
                  <i class="fa fa-arrow-right nav-icon"></i>
                  <p>View Video List</p>
                </a>
              </li>
               
            </ul>
          </li>

          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa fa-users"></i>
              <p>
                 Placement
                 <i class="fas fa-angle-left right"></i>
  
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{('/admin/add-placement')}}" class="nav-link">
                  <i class="fa fa-arrow-right nav-icon"></i>
                  <p>Add Placement</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="{{('/admin/view-placements')}}" class="nav-link">
                  <i class="fa fa-arrow-right nav-icon"></i>
                  <p>View Placement</p>
                </a>
              </li>
               
            </ul>
          </li>

          <li class="nav-item">
            <a href="{{('/admin/view-enquiries')}}" class="nav-link">
              <i class="nav-icon fas fa fa-users"></i>
              <p>
                 Enquiry / Feedback
                 <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">  </span>
 
              </p>
            </a>
          </li>
           -->

          
         
      </nav>
      </div>
    
  </aside>
