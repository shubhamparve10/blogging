<footer class="theme-footer" style="margin-bottom: 500px;">
	<div class="container">
		<div class="logo"><a href="{{('/')}}"><img src="{{url('images/logo/logo.png')}}" alt=""></a></div>
		<ul class="social-icon">
			<li class="icon"><a href="#"><i class="fab fa-spotify"></i></a></li>
			<!-- <li class="icon"><a href="#"><i class="fab fa-twitter"></i></a></li> -->
			<li class="icon"><a href="https://www.youtube.com/channel/UCA6qp76Pk_3gr92yqZUyCog"><i class="fab fa-youtube"></i></a></li>
			<li class="icon"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
		</ul>
		<p class="copyright">&copy; Copyright Zion's Touch ©️ 2021 - All Rights Reserved.</p>
	</div> <!-- /.container -->
</footer> <!-- /.theme-footer -->

<button class="scroll-top tran3s">
	<i class="fa fa-angle-up" aria-hidden="true"></i>
</button>
