	<!--
			=============================================
				Top Header
			==============================================
			-->
			<div class="top-header">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-12">
							<!-- <div class="news-bell">Recent Post :</div> -->
							<!-- <div class="breaking-news easyTicker">
								<div class="wrapper">
									<div class="list"><a href="blog-details-v1.html">One morning, when Gregor Samsa woke from troubled dreams.</a></div>
									<div class="list"><a href="blog-details-v1.html">CNN's Kyung Lah sits down with Japan's World Cup-winning.</a></div>
									<div class="list"><a href="blog-details-v1.html">Song Byeok had every reason to be pleased with his success.</a></div>
								</div>
                            </div>  -->
                            <!-- /.breaking-news -->
						</div>
						<div class="col-lg-4 col-12">
							<ul class="social-icon text-right">

								<li class="icon"><a href="#"><i class="fab fa-spotify"></i></a></li>
 								<li class="icon"><a href="https://www.youtube.com/channel/UCA6qp76Pk_3gr92yqZUyCog"><i class="fab fa-youtube"></i></a></li>
                                <li class="icon"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>

								<!-- <li class="search-button">
									<button class="search" id="search-button"><i class="fas fa-search"></i></button>
								</li> -->
							</ul>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.top-header -->



			<!--
			=============================================
				Theme Header
			==============================================
			-->






<header class="theme-main-header">
				<div class="container">
					<div class="content-holder clearfix">
						<!-- <div class="logo"><a href="{{('/')}}"><img src="images/logo/logo.png"   alt=""></a></div> -->
						<div class="logo"><a href="{{('/')}}"><img src="{{url('images/logo/logo.png')}}"    alt=""></a></div>

						<!-- ============== Menu Warpper ================ -->
				   		<div class="menu-wrapper">
				   			<nav id="mega-menu-holder" class="clearfix">
							   <ul class="clearfix">

                               <li><a href="{{('/')}}">Blogs</a></li>
                                  <li><a href="{{('/about')}}">About</a></li>
                                  <li><a href="{{('/contact')}}">Contact</a></li> 
							   </ul>

							   
							</nav> <!-- /#mega-menu-holder -->
				   		</div> <!-- /.menu-wrapper -->
					</div> <!-- /.content-holder -->
				</div> <!-- /.container -->
			</header>
<!-- End Main Header -->


<!--Mobile Menu-->
