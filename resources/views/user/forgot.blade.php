@extends('layouts.app')

@section('content')
<section class="contact-section mt-5 patternimg">
    <div class="auto-container">
        <div class="row justify-content-md-center mt-5">
            <div class="col-md-7">
                @if(Session::has('flash_message_error'))
                    <div class="alert alert-dismissible fade show respo" role="alert" style="background-color: #CC9966;">
                    <p style="color:#fff;">{!! session('flash_message_error') !!}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                @endif
            </div>
            <div class="col-md-7">
                <div class="contact-widget ls-widget">
                    <div class="widget-title">
                        <h4><span class="icon flaticon-call"></span>Reset Password</h4>
                    </div>
                    <div class="widget-content">
                        <!-- Comment Form -->
                        <div class="default-form">
                            <!--Comment Form-->
                            <form method="POST" action="{{ url('/forgot_password') }}">
                                @csrf
                                <div class="row clearfix ">
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                        <input placeholder="Enter Your Email Here" type="email" class="form-control"  name="email"  required autocomplete="email" autofocus>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                        <button class="theme-btn btn-style-two" type="submit" id="submit" name="submit-form">{{ __('Reset Password') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
@endsection
