
@extends('layouts.frontLayout.front_design')
@section('content')
			<!-- ===================================================
				About Me Wrapper
			==================================================== -->
			<div class="contact-us">
			<div class="back-to-home1" ><a href="{{ url('/') }}" style="color:black"> <button class="theme-button-one"><i class="flaticon-left-arrow"></i> &nbsp;Back to Blog </button></a></div>
				<div class="main-text-wrapper">
					<div class="container">
						<div class="contact-form">
							<h2>Contact</h2>
							<p>You can contact me almost about anything</p>

							@if(Session::has('flash_message_error'))
								<div class="alert alert-error alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>{!! session('flash_message_error') !!}</strong>
								</div>
							@endif
							@if(Session::has('flash_message_success'))
								<div class="alert alert-success alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>{!! session('flash_message_success') !!}</strong>
								</div>
							@endif
								<form action="{{ url('/contact') }}" method="POST" autocomplete="off">
                                @csrf
								<label>full Name</label>
								<input type="text" placeholder="John Doe" name="name" autofocus required>
								<label>Email address</label>
								<input type="email" placeholder="ex@example.com" name="email" required>
								<label>Your message</label>
								<textarea placeholder="Message" name="subject" required></textarea>
								<button class="theme-button-one" type="submit">Submit</button>
							</form>
						</div> <!-- /.contact-form -->
					</div> <!-- /.container -->
				</div> <!-- /.main-text-wrapper -->
				<!--Contact Form Validation Markup -->
				<!-- Contact alert -->
				<div class="alert-wrapper" id="alert-success">
					<div id="success">
						<button class="closeAlert"><i class="fas fa-window-close"></i></button>
						<div class="wrapper">
			               	<p>Your message was sent successfully.</p>
			             </div>
			        </div>
			    </div> <!-- End of .alert_wrapper -->
			    <div class="alert-wrapper" id="alert-error">
			        <div id="error">
			           	<button class="closeAlert"><i class="fas fa-window-close"></i></button>
			           	<div class="wrapper">
			               	<p>Sorry!Something Went Wrong.</p>
			            </div>
			        </div>
			    </div> <!-- End of .alert_wrapper -->
			</div> <!-- /.contact-us -->



			@endsection
