<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- For Window Tab Color -->
		<!-- Chrome, Firefox OS and Opera -->
		<meta name="theme-color" content="#2c2c2c">
		<!-- Windows Phone -->
		<meta name="msapplication-navbutton-color" content="#2c2c2c">
		<!-- iOS Safari -->
		<meta name="apple-mobile-web-app-status-bar-style" content="#2c2c2c">

		<title>SOTTO - Blog and Portfolio HTML Template</title>

		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="images/fav-icon/icon.png">


		<!-- Main style sheet -->
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<!-- responsive style sheet -->
		<link rel="stylesheet" type="text/css" href="css/responsive.css">
		<!-- Theme-Color css -->
		<link rel="stylesheet" id="jssDefault" href="css/color.css">


		<!-- Fix Internet Explorer ______________________________________-->

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif]-->

			
	</head>

	<body>
		<div class="search-box" id="searchWrapper">
			<div id="close-button"></div>
	   		<div class="container">
	   			<form action="#">
	   				<div class="greeting-text"><span class="greeting"></span></div>
	   				<div class="input-wrapper">
	   					<input type="text" placeholder="type your keyword" autofocus>
	   				</div>
	   			</form>
	   		</div>
	   	</div> <!-- /.search-box -->


		<div class="main-page-wrapper">
			<!-- ===================================================
				Loading Transition
			==================================================== -->
			<div id="loader-wrapper">
				<div id="loader">
	                <span></span>
	                <span></span>
	                <span></span>
	                <span></span>
	                <span></span>
	            </div>
			</div>


			 


			<!-- 
			=============================================
				Top Header
			============================================== 
			-->
			<div class="top-header">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-12">
							<div class="news-bell">Recent Post :</div>
							<div class="breaking-news easyTicker">
								<div class="wrapper">
									<div class="list"><a href="blog-details-v1.html">One morning, when Gregor Samsa woke from troubled dreams.</a></div>
									<div class="list"><a href="blog-details-v1.html">CNN's Kyung Lah sits down with Japan's World Cup-winning.</a></div>
									<div class="list"><a href="blog-details-v1.html">Song Byeok had every reason to be pleased with his success.</a></div>
								</div> <!-- /.wrapper -->
							</div> <!-- /.breaking-news -->
						</div>
						<div class="col-lg-4 col-12">
							<ul class="social-icon text-right">
								<li class="icon"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li class="icon"><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li class="icon"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="icon"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
								<li class="search-button">
									<button class="search" id="search-button"><i class="fas fa-search"></i></button>
								</li>
							</ul>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.top-header -->

			

			<!-- 
			=============================================
				Theme Header
			============================================== 
			-->
			<header class="theme-main-header">
				<div class="container">
					<div class="content-holder clearfix">
						<div class="logo"><a href="index.html"><img src="images/logo/logo.png" alt=""></a></div>
						<!-- ============== Menu Warpper ================ -->
				   		<div class="menu-wrapper">
				   			<nav id="mega-menu-holder" class="clearfix">
							   <ul class="clearfix">
							      <li><a href="#">Home</a>
							      	<ul class="dropdown">
							            <li><a href="index.html">Home Standard</a></li>
							            <li><a href="index-2.html">Home Clasic</a></li>
							            <li><a href="index-3.html">Home Video Blog</a></li>
							            <li><a href="index-4.html">Home fullwidth</a></li>
							            <li><a href="index-5.html">Home masonry</a></li>
							            <li><a href="index-6.html">Home Image blog</a></li>
							        </ul>
							      </li>
							      <li class="active"><a href="#">Features</a>
							      	<ul class="dropdown">
							      		<li class="active"><a href="blog-v1.html">Blog Right Sidebar</a></li>
							      		<li><a href="blog-v2.html">Blog left Sidebar</a></li>
							      		<li><a href="blog-v3.html">BLOG STANDARD</a></li>
							      		<li><a href="blog-v4.html">BLOG MASONRY</a></li>
							            <li><a href="blog-v5.html">Blog IMAGE COVER</a></li>
							            <li><a href="blog-v6.html">BLOG FULLWIDTH</a></li>
							            <li><a href="blog-v7.html">Video Blog</a></li>
							            <li><a href="#">Other layout</a>
							      			<ul>
							      				<li><a href="archives.html">Date archives</a></li>
							      				<li><a href="catergories.html">catergories archives</a></li>
							      			</ul>
							      		</li>
							         </ul>
							      </li>
							      <li><a href="#">Single post</a>
							      	<ul class="dropdown">
							            <li><a href="blog-details-v1.html">Standard Single post</a></li>
							            <li><a href="blog-details-v2.html">Gallery single post</a></li>
							            <li><a href="blog-details-v3.html">Gallery with lightbox</a></li>
							            <li><a href="blog-details-v4.html">Video post</a></li>
							            <li><a href="blog-details-v5.html">Fullwidth post</a></li>
							            <li><a href="blog-details-v6.html">Classic Single post</a></li>
							         </ul>
							      </li>
							      <li><a href="about-me.html">About me</a></li>
							      <li><a href="contact.html">contact me</a></li>
							   </ul>
							</nav> <!-- /#mega-menu-holder -->
				   		</div> <!-- /.menu-wrapper -->
					</div> <!-- /.content-holder -->
				</div> <!-- /.container -->
			</header> <!-- /.theme-main-header -->

			
			<!-- 
			=============================================
				Theme Inner Banner
			============================================== 
			-->
			<div class="theme-inner-banner bg-box section-margin-bottom">
				<div class="container">
					<h2>Blog right Sidebar</h2>
					<ul>
						<li><a href="index.html">Home</a></li>
						<li>/</li>
						<li>Blog</li>
					</ul>
				</div> <!-- /.container -->
			</div> <!-- /.theme-inner-banner -->
			
			
			
			
			<!-- 
			=============================================
				Main Blog Post Wrapper
			============================================== 
			-->
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-12 blog-grid-style hover-effect-one">
						<div class="single-blog-post">
							<div class="image-box"><img src="images/blog/1.jpg" alt=""></div>
							<div class="post-meta-box bg-box">
								<ul class="author-meta clearfix">
									<li class="tag"><a href="catergories.html">Lifestyle</a></li>
									<li class="date"><a href="archives.html">15 may , 2018</a></li>
								</ul>
								<h4 class="title"><a href="blog-details-v1.html">This is a standard post with an image</a></h4>
								<ul class="share-meta clearfix">
									<li><a href="#"><i class="icon flaticon-comment"></i>Comments (04)</a></li>
									<li><a href="#"><i class="icon flaticon-like-heart"></i>Likes (02)</a></li>
									<li class="share-option">
										<button><i class="icon flaticon-arrows"></i> Share</button>
										<ul class="share-icon">
											<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
											<li><a href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
											<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
									</li> <!-- /.share-option -->
								</ul>
								<p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.</p>
							</div> <!-- /.post-meta-box -->
						</div> <!-- /.single-blog-post -->
						<div class="single-blog-post">
							<div class="image-box"><img src="images/blog/2.jpg" alt=""></div>
							<div class="post-meta-box bg-box">
								<ul class="author-meta clearfix">
									<li class="tag"><a href="catergories.html">Travel</a></li>
									<li class="date"><a href="archives.html">14 may , 2018</a></li>
								</ul>
								<h4 class="title"><a href="blog-details-v1.html">This is a standard post with an image</a></h4>
								<ul class="share-meta clearfix">
									<li><a href="#"><i class="icon flaticon-comment"></i>Comments (03)</a></li>
									<li><a href="#"><i class="icon flaticon-like-heart"></i>Likes (05)</a></li>
									<li class="share-option">
										<button><i class="icon flaticon-arrows"></i> Share</button>
										<ul class="share-icon">
											<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
											<li><a href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
											<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
									</li> <!-- /.share-option -->
								</ul>
								<p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.</p>
							</div> <!-- /.post-meta-box -->
						</div> <!-- /.single-blog-post -->
						<div class="single-blog-post">
							<div class="image-box"><img src="images/blog/3.jpg" alt=""></div>
							<div class="post-meta-box bg-box">
								<ul class="author-meta clearfix">
									<li class="tag"><a href="catergories.html">Art</a></li>
									<li class="date"><a href="archives.html">13 may , 2018</a></li>
								</ul>
								<h4 class="title"><a href="blog-details-v1.html">This is a standard post with an image</a></h4>
								<ul class="share-meta clearfix">
									<li><a href="#"><i class="icon flaticon-comment"></i>Comments (13)</a></li>
									<li><a href="#"><i class="icon flaticon-like-heart"></i>Likes (25)</a></li>
									<li class="share-option">
										<button><i class="icon flaticon-arrows"></i>(03) Shares</button>
										<ul class="share-icon">
											<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
											<li><a href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
											<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
									</li> <!-- /.share-option -->
								</ul>
								<p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.</p>
							</div> <!-- /.post-meta-box -->
						</div> <!-- /.single-blog-post -->
						<div class="single-blog-post">
							<div class="image-box"><img src="images/blog/4.jpg" alt=""></div>
							<div class="post-meta-box bg-box">
								<ul class="author-meta clearfix">
									<li class="tag"><a href="catergories.html">Lifestyle</a></li>
									<li class="date"><a href="archives.html">10 may , 2018</a></li>
								</ul>
								<h4 class="title"><a href="blog-details-v1.html">This is a standard post with an image</a></h4>
								<ul class="share-meta clearfix">
									<li><a href="#"><i class="icon flaticon-comment"></i>Comments (07)</a></li>
									<li><a href="#"><i class="icon flaticon-like-heart"></i>Likes (10)</a></li>
									<li class="share-option">
										<button><i class="icon flaticon-arrows"></i>(01) Shares</button>
										<ul class="share-icon">
											<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
											<li><a href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
											<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
									</li> <!-- /.share-option -->
								</ul>
								<p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.</p>
							</div> <!-- /.post-meta-box -->
						</div> <!-- /.single-blog-post -->
						<div class="single-blog-post">
							<div class="image-box"><img src="images/blog/9.jpg" alt=""></div>
							<div class="post-meta-box bg-box">
								<ul class="author-meta clearfix">
									<li class="tag"><a href="catergories.html">Art</a></li>
									<li class="date"><a href="archives.html">10 may , 2018</a></li>
								</ul>
								<h4 class="title"><a href="blog-details-v1.html">This is a standard post with an image</a></h4>
								<ul class="share-meta clearfix">
									<li><a href="#"><i class="icon flaticon-comment"></i>Comments (02)</a></li>
									<li><a href="#"><i class="icon flaticon-like-heart"></i>Likes (01)</a></li>
									<li class="share-option">
										<button><i class="icon flaticon-arrows"></i>Share</button>
										<ul class="share-icon">
											<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
											<li><a href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
											<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
									</li> <!-- /.share-option -->
								</ul>
								<p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.</p>
							</div> <!-- /.post-meta-box -->
						</div> <!-- /.single-blog-post -->
						<div class="single-blog-post">
							<div class="image-box"><img src="images/blog/10.jpg" alt=""></div>
							<div class="post-meta-box bg-box">
								<ul class="author-meta clearfix">
									<li class="tag"><a href="catergories.html">Lifestyle</a></li>
									<li class="date"><a href="archives.html">05 may , 2018</a></li>
								</ul>
								<h4 class="title"><a href="blog-details-v1.html">This is a standard post with an image</a></h4>
								<ul class="share-meta clearfix">
									<li><a href="#"><i class="icon flaticon-comment"></i>Comments (10)</a></li>
									<li><a href="#"><i class="icon flaticon-like-heart"></i>Likes (15)</a></li>
									<li class="share-option">
										<button><i class="icon flaticon-arrows"></i>(05) Shares</button>
										<ul class="share-icon">
											<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
											<li><a href="#"><i class="fab fa-twitter"></i></a></li>
											<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
											<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
										</ul>
									</li> <!-- /.share-option -->
								</ul>
								<p>One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment.</p>
							</div> <!-- /.post-meta-box -->
						</div> <!-- /.single-blog-post -->

						<div class="theme-pagination text-center">
							<ul class="clearfix">
								<li><a href="#"><i class="icon flaticon-left-arrow"></i></a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#"><i class="icon flaticon-right-arrow"></i></a></li>
							</ul>
						</div> <!-- /.theme-pagination -->
					</div> <!-- /.col- -->
					
					<!-- ======================== Theme Sidebar =============================== -->
					<div class="col-lg-4 col-md-7 col-12 theme-main-sidebar">
						<div class="sidebar-box bg-box about-me">
							<h6 class="sidebar-title">About me</h6>
							<img src="images/home/1.jpg" alt="">
							<p>Hi, I am David Walmart. As for now I'm only focusing my attention on enjoyment. I'm being my true self with the values, dreams and goals that I have....</p>
							<div class="clearfix"><img src="images/home/sign.png" alt="" class="signature float-right"></div>
						</div> <!-- /.about-me -->
						<div class="sidebar-box bg-box sidebar-categories">
							<h6 class="sidebar-title">Categories</h6>
							<ul>
								<li><a href="catergories.html">Web Design</a></li>
								<li><a href="catergories.html">Fashion</a></li>
								<li><a href="catergories.html">Graphic Design</a></li>
								<li><a href="catergories.html">News and Events</a></li>
								<li><a href="catergories.html">Personal</a></li>
								<li><a href="catergories.html">Branding Agency</a></li>
								<li><a href="catergories.html">Lifestyle</a></li>
								<li><a href="catergories.html">Travel</a></li>
							</ul>
						</div> <!-- /.sidebar-categories -->
						<div class="sidebar-box bg-box sidebar-trending-post">
							<h6 class="sidebar-title">Trending Post</h6>
							<div class="single-trending-post clearfix">
								<img src="images/blog/5.jpg" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="blog-details-v2.html">New Benefits For Small Business </a></h6>
									<ul>
										<li class="tag">lifestyle</li>
										<li class="date">16 march</li>
									</ul>
								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->
							<div class="single-trending-post clearfix">
								<img src="images/blog/6.jpg" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="blog-details-v2.html">New Benefits For Small Business </a></h6>
									<ul>
										<li class="tag">art</li>
										<li class="date">15 march</li>
									</ul>
								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->
							<div class="single-trending-post clearfix">
								<img src="images/blog/7.jpg" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="blog-details-v2.html">New Benefits For Small Business </a></h6>
									<ul>
										<li class="tag">travel</li>
										<li class="date">13 march</li>
									</ul>
								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->
							<div class="single-trending-post clearfix">
								<img src="images/blog/8.jpg" alt="" class="float-left">
								<div class="post float-left">
									<h6><a href="blog-details-v2.html">New Benefits For Small Business </a></h6>
									<ul>
										<li class="tag">life</li>
										<li class="date">10 march</li>
									</ul>
								</div> <!-- /.post -->
							</div> <!-- /.single-trending-post -->
						</div> <!-- /.sidebar-trending-post -->
						<div class="sidebar-box bg-box sidebar-categories">
							<h6 class="sidebar-title">Archives</h6>
							<ul>
								<li><a href="archives.html">May 2018</a></li>
								<li><a href="archives.html">April 2018</a></li>
								<li><a href="archives.html">March 2018</a></li>
								<li><a href="archives.html">February 2018</a></li>
								<li><a href="archives.html">January 2018</a></li>
								<li><a href="archives.html">December 2107</a></li>
								<li><a href="archives.html">November 2017</a></li>
								<li><a href="archives.html">October 2017</a></li>
							</ul>
						</div> <!-- /.sidebar-categories -->
						<div class="sidebar-box bg-box sidebar-tags">
							<h6 class="sidebar-title">Keyword</h6>
							<ul class="clearfix">
								<li><a href="catergories.html">Design</a></li>
								<li><a href="catergories.html">Fashion</a></li>
								<li><a href="catergories.html">Graphic</a></li>
								<li><a href="catergories.html">News</a></li>
								<li><a href="catergories.html">Personal</a></li>
								<li><a href="catergories.html">Branding</a></li>
								<li><a href="catergories.html">Lifestyle</a></li>
								<li><a href="catergories.html">Travel</a></li>
							</ul>
						</div> <!-- /.sidebar-tags -->
						<div class="sidebar-box bg-box sidebar-newsletter">
							<h6 class="sidebar-title">newsletter</h6>
							<form action="#">
								<input type="email" placeholder="Your email address" required>
								<button class="theme-button-one">Sign up</button>
							</form>
						</div> <!-- /.sidebar-newsletter -->
					</div> <!-- /.theme-main-sidebar -->
				</div> <!-- /.row -->
			</div> <!-- /.container -->


			<!--
			=====================================================
				Footer
			=====================================================
			-->
			<footer class="theme-footer">
				<div class="container">
					<div class="logo"><a href="index.html"><img src="images/logo/logo.png" alt=""></a></div>
					<p class="footer-text">Blog and Portfolio Theme</p>
					<ul class="social-icon">
						<li class="icon"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li class="icon"><a href="#"><i class="fab fa-twitter"></i></a></li>
						<li class="icon"><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
						<li class="icon"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
					<p class="copyright">&copy; Copyright SOTTO - Powered by heloshape.</p>
				</div> <!-- /.container -->
			</footer> <!-- /.theme-footer -->
			

	        

	        <!-- Scroll Top Button -->
			<button class="scroll-top tran3s">
				<i class="fa fa-angle-up" aria-hidden="true"></i>
			</button>
			


		<!-- Optional JavaScript _____________________________  -->

    	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    	<!-- jQuery -->
		<script src="vendor/jquery.2.2.3.min.js"></script>
		<!-- Popper js -->
		<script src="vendor/popper.js/popper.min.js"></script>
		<!-- Bootstrap JS -->
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<!-- Style-switcher  -->
		<script src="vendor/jQuery.style.switcher.min.js"></script>
		<!-- jquery-easy-ticker-master -->
		<script src="vendor/jquery-easy-ticker-master/jquery.easy-ticker.min.js"></script>
		<!-- jquery easing -->
		<script src="vendor/jquery.easing.1.3.js"></script>
		<!-- Font Awesome -->
		<script src="fonts/font-awesome/fontawesome-all.min.js"></script>
	    <!-- menu  -->
		<script src="vendor/menu/src/js/jquery.slimmenu.js"></script>
		<!-- owl.carousel -->
		<script src="vendor/owl-carousel/owl.carousel.min.js"></script>
		<!-- Fancybox -->
		<script src="vendor/fancybox/dist/jquery.fancybox.min.js"></script>
		<!-- Youtube Video -->
		<script src="vendor/jquery.fitvids.js"></script>

		<!-- Theme js -->
		<script src="js/theme.js"></script>
		</div> <!-- /.main-page-wrapper -->
	</body>
</html>