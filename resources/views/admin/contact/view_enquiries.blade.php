@extends('layouts/backLayout/back_design')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>
.view {
    margin: 0 auto; /* Added */
    float: none; /* Added */
    margin-bottom: 10px; /* Added */
    top:30px;
    width:40%;
}</style>
@section('content')
 
<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Blog Section</h1>
            @if(Session::has('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{!! session('flash_message_error') !!}</strong>
          </div>
      @endif   
      @if(Session::has('flash_message_success'))
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{!! session('flash_message_success') !!}</strong>
          </div>
      @endif   
          </div> 
        </div> 
      </div> 
    </div>   


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"> All Enquiries/Feedback  </h3>
              

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile No.</th>
                  <th>Subject</th>
                  <th>Message</th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  @foreach($allEnquiries as $enquiry)
                <tr class="gradeX">
                  <td>{{ $loop->index+1 }}</td>
                  <!-- <td style="text-align:center;"><span style="display: none;">{{ $loop->index+1 }}</span>{{ $enquiry->id }}</td> -->
                  <td style="text-align:center;">{{ $enquiry->name }}</td>
                  <td style="text-align:center;">{{ $enquiry->email }}</td>
                  <td style="text-align:center;">{{ $enquiry->mobile }}</td>
                  <td>{{ $enquiry->subject }}</td>
                  <td>{{ $enquiry->comment }}</td>
                  <td class="center " style="text-align: center !important; width: 25%;">
                    <a href="#myModal{{ $enquiry->id }}" data-toggle="modal" class="btn btn-success btn-mini" title="View Blog">View</a>
            
                    <a  href="{{ url('admin/delete-enquiry/'.$enquiry->id) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-mini delete-confirm" title="Delete product">Delete</a>
                 </td>
                </tr>
                <div id="myModal{{ $enquiry->id }}" class="modal hide">
                
                    <div class="view" style="width: 70rem;" id="myModal{{ $enquiry->id }}">
                      <div class="card-body " style="background: beige;">
                      <button data-dismiss="modal" class="close" type="button">×</button>

                      <b>Name : </b>    <h3>{{ $enquiry->name }}  </h3><br>
                      <b>Email : </b>    <h3>{{ $enquiry->email }}  </h3><br>
                      <b>Mobile : </b>    <h3>{{ $enquiry->mobile }}  </h3><br>
                      <b>Subject : </b>    <h3>{{ $enquiry->subject }}  </h3><br>
                      <b>Comment : </b>    <h3>{{ $enquiry->comment }}  </h3><br>

                       
                      </div>
                     
                  </div>
 
                </div>  
                @endforeach
                   
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>


@endsection


 








