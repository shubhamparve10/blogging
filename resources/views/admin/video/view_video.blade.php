@extends('layouts/backLayout/back_design')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>
    .view {
        margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 50px; /* Added */
        top:60px;
        width:40%;
}</style>

@section('content')

<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Blog Section</h1>
            @if(Session::has('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{!! session('flash_message_error') !!}</strong>
          </div>
      @endif   
      @if(Session::has('flash_message_success'))
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{!! session('flash_message_success') !!}</strong>
          </div>
      @endif   
          </div> 
        </div> 
      </div> 
    </div>
    <!-- /.content-header -->

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
            <!-- /.card -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"> All videos</h3>
                <a href="{{ url('/admin/add_video') }}"><button style="float: right; margin: 3px 3px;" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Video</button></a>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>#</th>                   
                  <th>Title</th>
                  <th>Link</th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  @foreach($videos as $video)
                <tr class="gradeX">
                <td style="text-align: center;">{{ $loop->index+1 }} </td>
                   <td style="text-align: center;">{{ $video->title }}</td>
                  <td style="text-align: center;"><a href="{{ $video->link }}" target="_blank">{{ $video->link }}</a></td>
                

                  <td class="center" style="text-align: center !important;">
                    <a href="#myModal{{ $video->id }}" data-toggle="modal" class="btn btn-success btn-mini" title="View Product">View</a>
                    <a href="{{ url('/admin/edit_video/'.$video->id) }}" class="btn btn-primary btn-mini" title="Edit Product ">Edit</a>
                   
                    <a id="delVideo" href="{{ url('/admin/delete_video/'.$video->id) }}" class="btn btn-danger btn-mini delete-confirm" title="Delete product">Delete</a>
                    
                 </td>
                </tr>
                <div id="myModal{{ $video->id }}" class="modal hide">
                
                    <div class="view" style="width: 70rem;" id="myModal{{ $video->id }}">
                      <div class="card-body " style="background: beige;">
                      <button data-dismiss="modal" class="close" type="button">×</button>
                      <b>Name : </b>    <h3>{{ $video->id }}  </h3><br>
                      <b>Title : </b>  <h3>{{ $video->title }} </h3><br>
                      <iframe width="100%" height="250px" src="https://www.youtube.com/embed/{{ $video->link }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 
                      </div>
                     
                  </div>
 
                </div>  
                @endforeach
                   
                  
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
 
</div>
<script>
    $('.delete-confirm').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Are you sure you want to delete this Video?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
            window.location.href = url;
        }
    });
});
</script>
@endsection




