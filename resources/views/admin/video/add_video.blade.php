@extends('layouts/backLayout/back_design')
@section('content')
<div class="content-wrapper">
     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0">Video Section</h1>
            @if(Session::has('flash_message_error'))
          <div class="alert alert-error alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{!! session('flash_message_error') !!}</strong>
          </div>
      @endif   
      @if(Session::has('flash_message_success'))
          <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{!! session('flash_message_success') !!}</strong>
          </div>
      @endif   
          </div> 
        </div> 
      </div> 
    </div>



<section class="content">
      <div class="container-fluid">
       <div class="card card-primary">
              <!-- <div class="card-header">
                <h3 class="card-title">Add Video</h3>
              </div> -->
              <!-- /.card-header -->
              <!-- form start -->
              <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/add_video') }}" name="add_banner" > 
              {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1"> Video Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title"required>
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputPassword1">Add URL</label>
                    <input type="text" class="form-control" id="link" name="link" placeholder="Add URL">
                  
                  </div> -->
                  <div class="input-group-prepend form-group">
                          <span class="input-group-text" id="basic-addon3">https://www.youtube.com/watch?v=</span>
                          <input type="text" class="form-control" id="link" name="link" placeholder="eg. wHYaDqPQGD8"required>

                        </div>
                 
                 
                <!-- /.card-body -->

                <div class="card-footer">
                <a href="{{('/admin')}}" class="btn btn-secondary">Cancel</a>

                  <button  type="submit" value="Add Banner" class="btn btn-primary float-right">Submit</button>
                </div>
              </form>
            </div>
      </div>
      </div> 
</section>
</div>
          <!-- /.card -->

@endsection








<!-- <div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Banners</a> <a href="#" class="current">Add Banner</a> </div>
    <h1>Banners Section</h1>
    @if(Session::has('flash_message_error'))
        <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{!! session('flash_message_error') !!}</strong>
        </div>
    @endif   
    @if(Session::has('flash_message_success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{!! session('flash_message_success') !!}</strong>
        </div>
    @endif   
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add Banner</h5>
          </div>
          <div class="widget-content nopadding">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/add-banner') }}" name="add_banner" id="add_banner" novalidate="novalidate"> {{ csrf_field() }}
              
               
               
               <div class="control-group">
                <label class="control-label">Title : </label>
                <div class="controls">
                <input type="text"   name="title" id="title"   class="form-control" required>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Link : </label>
                <div class="controls">
                  <input type="text" name="link" id="link"  >
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Enable : </label>
                <div class="controls">
                  <input type="checkbox" name="status" id="status" class="btn btn-success" value="1">
                </div>
              </div>
              <div class="control-group">
                <div class="form-actions" style="float: right;">
                  <input type="submit" value="Add Banner" class="btn btn-success">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 -->
