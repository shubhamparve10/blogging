@extends('layouts.frontLayout.front_design')
@section('content')

<!-- Banner Section -->
<section class="banner-section banner-one">
    <div class="left-based-text">
        <div class="base-inner">
            <div class="hours">
                <ul class="clearfix">
                    <li><a href="https://www.facebook.com/govind.chandak.50"><span class="fab fa-facebook-square"></span>
                    </a></li>
                    <li><a href="https://www.youtube.com/channel/UC_0Yew6BJoD4AlnlQXc8a5Q"><span class="fab fa-youtube">
                    </span></a></li>
                    <li><a href="https://www.instagram.com/govind_chandak2012/"><span class="fab fa-instagram"></span></a>
                    </li>
                                
                 </ul>
            </div>
            <!--<div class="social-links">-->
            <!--    <ul class="clearfix">-->
            <!--        <li><a href="#"><span>Twitter</span></a></li>-->
            <!--        <li><a href="#"><span>Facebook</span></a></li>-->
            <!--        <li><a href="#"><span>Youtube</span></a></li>-->
            <!--    </ul>-->
            <!--</div>-->

        </div>
    </div>

    <div class="banner-carousel owl-theme owl-carousel">
        <!-- Slide Item -->
        <div class="slide-item">
            <div class="image-layer" style="background-image: url(images/frontend_images/main-slider/23.jpg)"></div>
            <div class="left-top-line"></div>
            <div class="right-bottom-curve"></div>
            <div class="right-top-curve"></div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="content">
                        <div class="inner">
                            <div class="sub-title">Welcome to Matrix Digital Technologies</div>
                            <h1>Grooming For<br>Digital Success</h1>
                            <div class="link-box">
                                <a class="theme-btn btn-style-one" href="{{url('courses/')}}">
                                    <i class="btn-curve"></i>
                                    <span class="btn-title">Know more</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide-item">
            <div class="image-layer" style="background-image: url(images/frontend_images/main-slider/24.jpg)"></div>
            <div class="left-top-line"></div>
            <div class="right-bottom-curve"></div>
            <div class="right-top-curve"></div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="content">
                        <div class="inner">
                            <div class="sub-title">Welcome to Matrix Digital Technologies</div>
                            <h1>Grooming For<br>Digital Success</h1>
                            <div class="link-box">
                                <a class="theme-btn btn-style-one" href="{{url('courses/')}}">
                                    <i class="btn-curve"></i>
                                    <span class="btn-title">Know More</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Slide Item -->
        <div class="slide-item">
            <div class="image-layer" style="background-image: url(images/frontend_images/main-slider/25.jpg)"></div>
            <div class="left-top-line"></div>
            <div class="right-bottom-curve"></div>
            <div class="right-top-curve"></div>
            <div class="auto-container">
                <div class="content-box">
                    <div class="content">
                        <div class="inner">
                            <div class="sub-title">Welcome to Matrix Digital Technologies</div>
                            <h1>Grooming For<br>Digital Success</h1>
                            <div class="link-box">
                                <a class="theme-btn btn-style-one" href="{{url('courses/')}}">
                                    <i class="btn-curve"></i>
                                    <span class="btn-title">Know More</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        


    </div>
</section>
<!--End Banner Section -->

<!--Services Section-->
<section class="services-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Title Block-->
            <div class="title-block col-xl-6 col-lg-12 col-md-12 col-sm-12">
                <div class="inner">
                    <div class="sec-title">
                        <h2>We Shape the Perfect <br>Solutions</h2>
                        <div class="lower-text">We are committed to providing our customers with exceptional
                            service while offering our employees the best training.</div>
                    </div>
                </div>
            </div>
            <!--Service Block-->
            <div class="service-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="0ms"
                data-wow-duration="1500ms">
                <div class="inner-box">
                    <div class="bottom-curve"></div>
                    <div class="icon-box"><span class="flaticon-responsive"></span></div>
                    <h6>Fundamentals Of <br>Digital Marketing</h6>
                </div>
            </div>
            <!--Service Block-->
            <div class="service-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft"
                data-wow-delay="300ms" data-wow-duration="1500ms">
                <div class="inner-box">
                    <div class="bottom-curve"></div>
                    <div class="icon-box"><span class="flaticon-chat-1"></span></div>
                    <h6>Blogging</h6>
                </div>
            </div>
            <!--Service Block-->
            <div class="service-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay="0ms"
                data-wow-duration="1500ms">
                <div class="inner-box">
                    <div class="bottom-curve"></div>
                    <div class="icon-box"><span class="flaticon-app-development"></span></div>
                    <h6>Website Planning<br>& Analysis</h6>
                </div>
            </div>
            <!--Service Block-->
            <div class="service-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft"
                data-wow-delay="300ms" data-wow-duration="1500ms">
                <div class="inner-box">
                    <div class="bottom-curve"></div>
                    <div class="icon-box"><span class="flaticon-stats"></span></div>
                    <h6>Web Analytics</h6>
                </div>
            </div>
            <!--Service Block-->
            <div class="service-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft"
                data-wow-delay="600ms" data-wow-duration="1500ms">
                <div class="inner-box">
                    <div class="bottom-curve"></div>
                    <div class="icon-box"><span class="flaticon-digital-marketing"></span></div>
                    <h6>Affiliate Marketing</h6>
                </div>
            </div>
            <!--Service Block-->
            <div class="service-block col-xl-3 col-lg-6 col-md-6 col-sm-12 wow fadeInLeft"
                data-wow-delay="900ms" data-wow-duration="1500ms">
                <div class="inner-box">
                    <div class="bottom-curve"></div>
                    <div class="icon-box"><span class="flaticon-desktop"></span></div>
                    <h6><a href="ui-designing.php">Growth Hacking</a></h6>
                </div>
            </div>
            <div class="col-xl-12 centered">
                <a class="theme-btn btn-style-one" href="{{url('about/')}}">
                    <i class="btn-curve"></i>
                    <span class="btn-title">Know More</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!--About Section-->
<section class="about-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Image Column-->
            <div class="image-column col-xl-6 col-lg-12 col-md-12 col-sm-12">
                <div class="inner">
                    <div class="image-block wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms"><img
                            src="{{ asset('images/frontend_images/main-slider/11.jpg') }}" alt="" style="height:380px;object-fit:cover"></div>
                    <div class="image-block wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms"><img
                            src="{{ asset('images/frontend_images/main-slider/12.jpg') }}" alt="" style="height:330px;object-fit:cover"></div>
                </div>
            </div>
            <!--Text Column-->
            <div class="text-column col-xl-6 col-lg-12 col-md-12 col-sm-12">
                <div class="inner">
                    <div class="sec-title">
                        <h2>Why Matrix Digital Technologies ?</h2>
                        <div class="lower-text" >Key Highlights Of The Training Programs.</div>
                    </div>                   
                    <div class="text clearfix">
                        <ul>
                            <li>6 Students in a batch so as to ensure personal attention.</li>
                            <li>1:1 Mentoring Sessions To Meet Your Personal Goals.</li>
                            <li>100% Placement Assistance In MNCs & High Growth Companies.</li>
                            <li>Certifications By Google, Facebook, IBM, Hubspot, Matrix &amp; Start Up India.</li>
                            <li>Lifetime LMS Access.</li>
                            <li>Resume & LinkedIn Profile Building.</li>
                            <li>Guest Lectures From Thought Leaders.</li>
                            <li>Mock Interviews To Crack Toughest Companies.</li>
                            <li>Priority Access To All Courses & Boot camps.</li>
                            <li>Regular Doubt &amp; Back Up Sessions</li>

                        </ul>
                        <!--<div class="since"><span class="txt">Since <br>2008</span></div>-->
                    </div>
                    <!--<div class="link-box">-->
                    <!--    <a class="theme-btn btn-style-one" href="{{url('about/')}}">-->
                    <!--        <i class="btn-curve"></i>-->
                    <!--        <span class="btn-title">Discover More</span>-->
                    <!--    </a>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
    </div>
</section>

<!--Live Section-->
<section class="live-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>Experience us live </h2>
        </div>
        <div class="main-image-box">
            <div class="image-layer" style="background-image: url(images/frontend_images/resource/featured-image-3.jpg);"></div>
            <div class="inner clearfix">
                <div class="round-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="round-inner">
                        <div class="vid-link">
                            <a href="https://youtu.be/boVEQpWGW-0" class="lightbox-image">
                                <div class="icon"><span class="flaticon-play-button-1"></span><i
                                        class="ripple"></i></div>
                            </a>
                        </div>
                        <div class="title">
                            <h3>Excited To Learn From Us?</h3>
                        </div>
                        <div class="more-link"><a href="{{url('about/')}}">Read More</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Funfacts Section -->
<section class="facts-section">
    <div class="image-layer" style="background-image: url(images/frontend_images/background/image-1.jpg);"></div>
    <div class="auto-container">
        <div class="inner-container">

            <!-- Fact Counter -->
            <div class="fact-counter">
                <div class="row clearfix">

                    <!--Column-->
                    <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                        <div class="inner">
                            <div class="content">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="3000" data-stop="2000">2000+</span><span>+</span>
                                </div>
                                <div class="counter-title">Seminars & Webinars</div>
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                        <div class="inner">
                            <div class="content">
                                <div class="count-outer count-box alternate">
                                    <span class="count-text" data-speed="3000" data-stop="90">90%</span><span>%</span>
                                </div>
                                <div class="counter-title">Placement Records</div>
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                        <div class="inner">
                            <div class="content">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="3000" data-stop="50">50+</span><span>+</span>
                                </div>
                                <div class="counter-title">College Partners PAN India</div>
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                        <div class="inner">
                            <div class="content">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="4000" data-stop="100">100+</span><span>+</span>
                                </div>
                                <div class="counter-title">Company Tie Ups</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Funfacts Section -->

<!-- workshop parrtners Section -->
<section class="sponsors-section">
    <div class="sponsors-outer">
        <div class="sec-title centered" style="max-width: 1000px !important;">
            <h2>Workshop Partners</h2>
        </div>
        <!--Sponsors-->
        <div class="auto-container">
            <!--Sponsors Carousel-->
            <div class="sponsors-carousel owl-theme owl-carousel">
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/24.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/2.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/3.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/4.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/5.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/6.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/7.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/8.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/9.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/10.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/11.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/12.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/13.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/14.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/15.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/16.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/17.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/18.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/19.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/21.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/22.png')}}" alt=""></figure>                    
                </div><div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/23.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/24.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/25.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/26.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/27.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/28.png')}}" alt=""></figure>                    
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/partners/29.png')}}" alt=""></figure>                    
                </div>
                
            
                
                     
            </div>
        </div>
    </div>
</section>

<!-- Parallax Section -->
<section class="parallax-section">
    <div class="image-layer" style="background-image: url(images/frontend_images/background/image-2.jpg);"></div>
    <div class="auto-container">
        <div class="content-box">
            <div class="icon-box"><span class="flaticon-app-development"></span></div>
            <h2>“GREAT THINGS IN
CARRER ARE NEVER DONE BY ONE PERSON. THEY’RE DONE BY A TEAM OF
PEOPLE”</h2>
        </div>
    </div>
</section>
<!-- End Funfacts Section -->

<!--Sponsors Section-->
<section class="sponsors-section">
    <div class="sponsors-outer">
        <div class="sec-title centered" style="max-width: 1000px !important;">
            <h2>Our Students Work At</h2>
        </div>
        <!--Sponsors-->
        <div class="auto-container">
            <!--Sponsors Carousel-->
            <div class="sponsors-carousel owl-theme owl-carousel">
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/1.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/2.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/3.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/4.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/5.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/6.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/7.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/8.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/9.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/10.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/11.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/12.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/13.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/14.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/15.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/16.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/17.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/18.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/19.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/20.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/21.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/22.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/23.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/24.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/25.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/26.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/27.png')}}" alt=""></figure>
                </div>
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/28.png')}}" alt=""></figure>
                </div> 
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/29.png')}}" alt=""></figure>
                </div> 
                <div class="slide-item">
                    <figure class="image-box"><img src="{{asset('images/frontend_images/clients/30.png')}}" alt=""></figure>
                </div> 
                
            </div>
        </div>
    </div>
</section>

<!-- News Section -->
<section class="news-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>Blogs</h2>
        </div>
        
         <div class="row clearfix">
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0ms"
                        data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="image-box">
                                <a href="{{url('/blog1')}}">
                                    <img src="{{asset('images/frontend_images/blogs/1.jpg')}}" alt="" style="height:280px;object-fit:cover">
                                    <!-- <iframe width="370" height="254" src="https://www.youtube.com/embed/3CQOnK9RjVE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                                </a>
                            </div>
                            <div class="lower-box">
                                <!--<div class="post-meta">-->
                                <!--    <ul class="clearfix">-->
                                        <!--<li><span class="far fa-clock"></span> 20 Mar</li>-->
                                        <!--<li><span class="far fa-user-circle"></span> Admin</li>-->
                                        <!--<li><span class="far fa-comments"></span> 2 Comments</li>-->
                                <!--    </ul>-->
                                <!--</div>-->
                                <h5><a href="{{url('/blog1')}}">Full Stack Digital Marketing Program</a></h5>
                                </div>
                        </div>
                    </div>
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="300ms"
                        data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="image-box">
                                <a href="{{url('/blog2')}}"><img src="{{asset('images/frontend_images/blogs/2.jpg')}}" alt="" style="height:280px;object-fit:cover"></a>
                            </div>
                            <div class="lower-box">
                                <!--<div class="post-meta">-->
                                <!--    <ul class="clearfix">-->
                                        <!--<li><span class="far fa-clock"></span> 20 Mar</li>-->
                                        <!--<li><span class="far fa-user-circle"></span> Admin</li>-->
                                        <!--<li><span class="far fa-comments"></span> 2 Comments</li>-->
                                <!--    </ul>-->
                                <!--</div>-->
                                <h5><a href="{{url('/blog2')}}">Growth of Digital Marketing </a></h5>
                                </div>
                        </div>
                    </div>
                    <!--News Block-->
                    <div class="news-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="600ms"
                        data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="image-box">
                                <a href="{{url('/blog3')}}"><img src="{{asset('images/frontend_images/blogs/3.jpg')}}" alt="" style="height:280px;object-fit:cover"></a>
                            </div>
                            <div class="lower-box">
                                <!--<div class="post-meta">-->
                                <!--    <ul class="clearfix">-->
                                        <!--<li><span class="far fa-clock"></span> 20 Mar</li>-->
                                        <!--<li><span class="far fa-user-circle"></span> Admin</li>-->
                                        <!--<li><span class="far fa-comments"></span> 2 Comments</li>-->
                                <!--    </ul>-->
                                <!--</div>-->
                                <h5><a href="{{url('/blog3')}}">What is digital marketing?</a></h5>
                                </div>
                        </div>
                    </div>
                </div>
    </div>
</section>

<!-- Call To Section -->
<!--<section class="call-to-section">-->
<!--    <div class="auto-container">-->
<!--        <div class="inner clearfix">-->
<!--            <div class="shape-1 wow slideInRight" data-wow-delay="0ms" data-wow-duration="1500ms"></div>-->
<!--            <div class="shape-2 wow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms"></div>-->
<!--            <h2>Let's Get Your Project <br>Started!</h2>-->
<!--            <div class="link-box">-->
<!--                <a class="theme-btn btn-style-two" href="{{url('about')}}">-->
<!--                    <i class="btn-curve"></i>-->
<!--                    <span class="btn-title">Contact with us</span>-->
<!--                </a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->


@endsection